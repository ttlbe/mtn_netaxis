import time,random
import Features.Resources.GroupLevelLocators
from selenium import webdriver
from Features.TestDataMtnScp import user,TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

import os, sys, inspect

from selenium.webdriver.support import expected_conditions as EC

from Features.TestDataMtnScp import TestDataMtnScp
from selenium.webdriver.common.action_chains import ActionChains


# # this function is called every time a new object of the base class is created.
# def __init__(self, driver):
#     self.driver=driver

# this function performs click on web element whose locator is passed to it.
def click(self, by_locator):
    WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator)).click()

# this function performs click on web element whose locator is passed to it.
def click_list(self, by_locator, position):
    e = WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located(by_locator))
    webdriver.ActionChains(self.driver).move_to_element(e[position] ).click(e[position] ).perform()

#return Length of Element List
def get_list_length(self, by_locator):
    e = WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located(by_locator))
    return(len(e))

#used as click for non dimensional elements  
def click_enoying_element(self, by_locator):
    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(by_locator)).click()

# this function asserts comparison of a web element's text with passed in text.
def assert_element_text(self, by_locator, element_text):
    web_element=WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))
    assert web_element.text == element_text

# this function performs text entry of the passed in text, in a web element whose locator is passed to it.
def enter_text(self, by_locator, text):
    return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator)).send_keys(text)

# this function checks if the web element whose locator has been passed to it, is enabled or not and returns
# web element if it is enabled.
def is_enabled(self, by_locator):
    return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))

# this function checks if the web element whose locator has been passed to it, is visible or not and returns
# true or false depending upon its visibility.
def is_visible(self,by_locator):
    element=WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))
    return bool(element)
# is invissible    
def is_invisible(self,by_locator):
    element=WebDriverWait(self.driver, 10).until(EC.invisibility_of_element(by_locator))
    return bool(element)

# this function moves the mouse pointer over a web element whose locator has been passed to it.
def hover_to(self, by_locator):
    element = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator))
    ActionChains(self.driver).move_to_element(element).perform()

# empties text fields of element whose locator has been passed to it
def clear_text_field(self, by_locator):
    return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(by_locator)).clear()

def move_to_element_and_click(self,by_locator):
    ActionChains(self.driver).move_to_element(by_locator).click(by_locator).perform()

#check if certain web element is visible and enabled so it is clickable
def is_clickable(self, by_locator):
    return WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(by_locator))

#check if element is selected by locator second argument takes boolean to compare with // TRUE
def check_if_selected_true(self, by_locator):
     return WebDriverWait(self.driver, 10).until(EC.element_located_selection_state_to_be(by_locator, True))


#check if element is selected by locator second argument takes boolean to compare with // TRUE
def check_if_selected_false(self, by_locator):
    return WebDriverWait(self.driver, 10).until(EC.element_located_selection_state_to_be(by_locator, False))

def check_check(self, by_locator):
    pass

#selects random element from list of elements
def c_select_random_element_from_list(self,css_locator):
    elements_list = WebDriverWait(self.driver, 10).until(lambda x: x.find_elements(By.CSS_SELECTOR, css_locator))
    random_lang= random.choice(elements_list)
    move_to_element_and_click(self,random_lang)

