import time
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver


############# LoginPage Locators ###########################


username_field = (By.ID, "loginEmail")
pw_field =(By.ID, "loginPassword")
submitBtn = (By.CSS_SELECTOR, ".btnSubmit")


########## GENARAL Locators ############################
popUp_msg_save_successful = (By.ID,"msgTemporary")
addGroupButton = (By.XPATH, "//a[contains(text(),'Add group')]")
