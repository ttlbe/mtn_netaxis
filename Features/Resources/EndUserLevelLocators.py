import time
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver


# ############ EndUser Locators #################################

eu_homePage_link=(By.CSS_SELECTOR,"home > a")
eu_callHistory_tile= (By.CSS_SELECTOR,".callHistory > .wrap")
eu_phonebook_tile= (By.CSS_SELECTOR,".phoneBook > .wrap")
eu_voicemail_tile= (By.CSS_SELECTOR,".voicemail > .wrap")
eu_forwardAllCalls_tile= (By.CSS_SELECTOR,"forwardCalls > .wrap")
eu_callCenters_tile= (By.CSS_SELECTOR,".callCenters > .wrap")
eu_doNotDisturb_tile = (By.CSS_SELECTOR, ".doNotDisturb")



eu_accountSettings_sideNav = (By.CSS_SELECTOR,"nav a[href='/settings/account']")
eu_myAnnouncements_sideNav = (By.CSS_SELECTOR,"nav a[href='/settings/announcements")
eu_MyDevices_sideNav = (By.CSS_SELECTOR,"nav a[href='/settings/devices")
eu_outgoingCalls_sideNav=(By.CSS_SELECTOR,"nav a[href='/settings/outgoing")
eu_incomingCalls_sideNav=(By.CSS_SELECTOR,"nav a[href='/settings/incoming")


##INCOMING CALLS PAGE
eu_rejectAnonymousCalls_toggle=(By.XPATH,"//section[@id='page']/main/div[2]/div[2]/div/div/div/label")
eu_doNotDisturb_toggle=(By.XPATH,"//section[@id='page']/main/div[3]/div[2]/div/div/div/label")
eu_forwardAllCalls_tab = (By.CSS_SELECTOR,"#tab-cfa .heading .selection")
eu_forw_num = (By.CSS_SELECTOR,"label[for='inpForwarding-1']")
eu_forw_off = (By.CSS_SELECTOR,"label[for='inpForwarding-0']")
eu_lineBusy_tab = (By.CSS_SELECTOR,"#tab-cfb .heading .selection")
eu_busy_off = (By.CSS_SELECTOR,"label[for='inpBusy-0']")
eu_busy_num = (By.CSS_SELECTOR,"label[for='inpBusy-1']")
eu_num_change = (By.CSS_SELECTOR, "div.control.jActive div.lnkSimple.lnkEdit a")
eu_keypad = (By.CSS_SELECTOR, "#ovrUserSelect li.jActive > a")
eu_keypad_input = (By.CSS_SELECTOR, "#tab-user-1 > div > section > input")
eu_keypad_save = (By.CSS_SELECTOR, "#ovrUserSelect div.btnSubmit > button")
eu_second_call = (By.CSS_SELECTOR, "label[for='inpSecondCalls']")
eu_callNotAns_tab = (By.CSS_SELECTOR,"#tab-cfna .heading .selection")
eu_noAns_off = (By.CSS_SELECTOR,"label[for='inpUnanswered-0']")
eu_noAns_num = (By.CSS_SELECTOR,"label[for='inpUnanswered-1']")
eu_timing_3 = (By.CSS_SELECTOR,"label[for='inpTiming-0']")
eu_timing_5 = (By.CSS_SELECTOR,"label[for='inpTiming-2']")
eu_active_timing_5 = (By.CSS_SELECTOR,"#tab-timing-1 div.jActive > div > label[for='inpTiming-2']")
eu_whenDeviceNotReachable_tab = (By.CSS_SELECTOR,"#tab-cfnr .heading .selection")
eu_notReachable_off = (By.CSS_SELECTOR,"label[for='inpUnreachable-0']")
eu_notReachable_num = (By.CSS_SELECTOR,"label[for='inpUnreachable-1']")
eu_voicemailsSettings_tab = (By.CSS_SELECTOR,"#tab-voicemail-1 > header > h3 > a")
eu_message_to_voice_inbox = (By.CSS_SELECTOR,"label[for='inpMessageTo-0']")
eu_message_to_mail = (By.CSS_SELECTOR,"label[for='inpMessageTo-1']")
eu_dont_Send_copy = (By.CSS_SELECTOR,"label[for='inpMessageCopyTo-0']")
eu_copy_to_mail = (By.CSS_SELECTOR,"label[for='inpMessageCopyTo-1']")
eu_success = (By.CSS_SELECTOR,"#msgTemporary")

##OUTGOING CALLS PAGE
eu_hideMyNr_toggle = (By.CSS_SELECTOR,"label[for='inpOutgoingPrivacy'")


##MY DEVICES PAGE
eu_addPersonalDevice_btn=(By.CSS_SELECTOR,".lnkAdd > a")
eu_deviceNameField_personalDevice=(By.ID,"inpDeviceName")
eu_phoneNrField_personalDevice=(By.ID,"inpDeviceNumber")
eu_saveBtn_addPersonalDevice=(By.CSS_SELECTOR,"#ovrPersonalDevice button")
eu_answerConfirm_toggle = (By.CSS_SELECTOR, "label[for='inpAnswerConfirmation']")
eu_answerConfirm_checkbox = (By.CSS_SELECTOR, "#inpAnswerConfirmation")
eu_new_personalDevice = (By.XPATH,"//div[@class='label' and contains(.,'testDevice')]")
eu_new_personalDevice_edit = (By.XPATH,"//div[@class='label' and contains(.,'testDevice')]/../../section/footer/div/div[1]/a")
eu_new_personalDevice_edited_delete = (By.XPATH,"//div[@class='label' and contains(.,'testDevice_edited')]/../../section/footer/div/div[2]/a")
eu_new_personalDevice_edited = (By.XPATH,"//div[@class='label' and contains(.,'testDevice_edited')]")
eu_personalDevice_error = (By.CSS_SELECTOR, "div.error")

##acount settings

eu_lang_eng = (By.CSS_SELECTOR, "label[for='inpLanguage-0']")
eu_lang_fr = (By.CSS_SELECTOR, "label[for='inpLanguage-2']")
eu_old_password = (By.CSS_SELECTOR, "#inpPassOld")
eu_new_password = (By.CSS_SELECTOR, "#inpPassNew")
eu_confirm_new_password = (By.CSS_SELECTOR, "#inpPassConfirm")
eu_password_change_save = (By.CSS_SELECTOR, "#tab-security-1 div.btnSubmit > button")
eu_change_password = (By.CSS_SELECTOR, "#tab-security-1 .selection")

## announcement

# eu_add_new_announcement = (By.CSS_SELECTOR, "//a[contains(.,'Add new announcement')]")
eu_add_new_announcement = (By.CSS_SELECTOR, ".lnkAdd > a")
eu_add_anc_name = (By.CSS_SELECTOR, "#inpAnnouncementName")
eu_rec_message = (By.CSS_SELECTOR, "label[for='inpAnnouncementCreate-1']")
eu_start_rec = (By.CSS_SELECTOR, "#inpRecord div.lnkSimple.lnkRecord > a")
eu_save_rec = (By.CSS_SELECTOR, "#ovrAnnouncementRecord div.btnSubmit > button")
eu_save_anc = (By.CSS_SELECTOR, "#ovrAnnouncement div.btnSubmit > button")
eu_new_anc = (By.XPATH,"//h3[contains(.,'test anc')]")
eu_edited_anc = (By.XPATH,"//h3[contains(.,'test anc_edited')]")
eu_anc_error = (By.CSS_SELECTOR, "#ovrAnnouncement  .error")
eu_edit_anc = (By.XPATH,"//h3[.='test anc']/../../footer/div[1]/a")
eu_delete_anc = (By.XPATH,"//h3[.='test anc_edited']/../../footer/div[2]/a")
eu_delete_anc_confirm = (By.CSS_SELECTOR,"#ovrAnnouncementDelete div.btnSubmit > button")

