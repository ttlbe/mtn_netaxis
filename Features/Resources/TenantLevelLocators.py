import time
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver




popUp_msg_save_successful = (By.ID,"msgTemporary")

######## SIDE NAV
homePage_tenantLvl=(By.CSS_SELECTOR,".home > a")
searchPage_tenantLvl=(By.CSS_SELECTOR,".search > a")
settingsPage_tenantLvl=(By.CSS_SELECTOR,".tenant-settings > a")
LicensesPage_tenantLvl=(By.CSS_SELECTOR,".licenses > a")
logOut_tenantLvl=(By.CSS_SELECTOR, ".userContainer a[href='/logout']")




### HOME PAGE 
addGroupButton = (By.XPATH, "//a[contains(text(),'Add group')]")
un_del_group = (By.XPATH, "//h3[contains(.,'UNDELETEABLEGROUP (grp_UNDELETEABLEGROUP_35647)')]")

### GROUP UNDER TENANT
licences_header = (By.XPATH, "//section[@id='tab-licenses']/header/h3/a")
edit_group_licences = (By.XPATH, "//a[contains(text(),'Edit licenses')]")
advanced_selector = (By.CSS_SELECTOR, ".serviceContainer:nth-child(2) .value label")
#advanced_selector = (By.CSS_SELECTOR, "#inpToggleInfinite_0")
basicCallAgent_selector = (By.CSS_SELECTOR, ".serviceContainer:nth-child(3) .value label")
fax2mail_selector = (By.CSS_SELECTOR, ".serviceContainer:nth-child(4) .value label")
flexseat_selector = (By.CSS_SELECTOR, ".serviceContainer:nth-child(5) .value label")
standard_selector = (By.CSS_SELECTOR, ".serviceContainer:nth-child(6) .value label")
standardCallCenterAgent_selector = (By.CSS_SELECTOR, ".serviceContainer:nth-child(7) .value label")
voicemail_selector = (By.CSS_SELECTOR, ".serviceContainer:nth-child(8) .value label")
licences_edit_header =  (By.XPATH, "//section[@id='ovrEditLicenses']/div/div/header/h2")

advanced_field = (By.CSS_SELECTOR, "#inpServicePack_0")
basicCallAgent_field = (By.CSS_SELECTOR, "#inpServicePack_1")
fax2mail_field = (By.CSS_SELECTOR, "#inpServicePack_2")
flexseat_field = (By.CSS_SELECTOR, "#inpServicePack_3")
standard_field = (By.CSS_SELECTOR, "#inpServicePack_4")
standardCallCenterAgent_field = (By.CSS_SELECTOR, "#inpServicePack_5")
voicemail_field = (By.CSS_SELECTOR, "#inpServicePack_6")

#### ADD GROUP FORM
addGroupForm_groupName_field = (By.CSS_SELECTOR, "#inpName")
addGroupForm_contactName_field = (By.CSS_SELECTOR, "#inpContactName")
addGroupForm_phoneNumber_field = (By.CSS_SELECTOR, "#inpContactPhoneNumber")
addGroupForm_email_field = (By.CSS_SELECTOR, "#inpContactEmailAddress")
addGroupForm_street_field =(By.CSS_SELECTOR, "#inpStreet")
addGroupForm_postalCode_field =(By.CSS_SELECTOR, "#inpPostalCode")
addGroupForm_city_field = (By.CSS_SELECTOR, "#inpCity")
addGroupForm_country_field = (By.CSS_SELECTOR, "#inpCountry")
addGroupForm_saveButton = (By.CSS_SELECTOR,".btnSubmit")



##### GROUP DETAILS PAGE
editBtn_editGroupForm = (By.XPATH,"//a[contains(.,'Edit')]")

continueAsGroupLink = (By.CSS_SELECTOR, ".impersonate > a") 

deleteGroupBtn = (By.PARTIAL_LINK_TEXT, "Delete group")    
confirmDeleteGroupBtn = (By.XPATH, "(//button[@type='submit'])[5]") 



##### SETTINGS PAGE
editBtn_editAdministratorDetails = (By.CSS_SELECTOR, "div.lnkEdit a")
administratorDetails_lastName_field = (By.CSS_SELECTOR, "input#inpLastName")
saveBtn_administratorDetails = (By.CSS_SELECTOR, "#ovrAddEditGroupAdmin .btnSubmit button")
changePsw_administratorDetails = (By.CSS_SELECTOR, ".actions .lnkEdit a")
administratorDetails_oldPsw_field = (By.CSS_SELECTOR, "#cInpOldPassword")
administratorDetails_newPsw_field = (By.CSS_SELECTOR , "#cInpPassword")
administratorDetails_repeatPsw_field = (By.CSS_SELECTOR , "#cInpRepeat")
saveBtn_administratorPassword = (By.CSS_SELECTOR, "#ovrPasswordAdjust .btnSubmit button")
phoneTab_phoneNumbers = (By.CSS_SELECTOR, "#tab-phoneNumbers h3 a")
rangesTable_phoneNumbers = (By.CSS_SELECTOR, ".tableContainer tBody tr")



##### SEARCH PAGE
search_btn = (By.CSS_SELECTOR, ".btnSubmit")
firstName_field = (By.CSS_SELECTOR, "#cInpFirstName")
lastName_field = (By.CSS_SELECTOR, "#cInpLastName")
phoneNumber_field = (By.CSS_SELECTOR, "#cInpSearchExtra")
groupId_table = (By.CSS_SELECTOR, ".tableContainer tbody tr td:nth-of-type(1) a")
searchTable_endUsers = (By.CSS_SELECTOR, ".tableContainer tbody tr")
email_field = (By.CSS_SELECTOR, "#cInpEmail")
userId_table = (By.CSS_SELECTOR, ".tableContainer tbody tr td:nth-of-type(2) a")




















