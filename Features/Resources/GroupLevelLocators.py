import time
from Features.TestDataMtnScp import *
from Features.Resources.BasePage import *
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
from pathlib import Path
import os

### GENERAL CONTENT
# filePath_audioFile="C:\\Users\\azizb\\Desktop\\python\\pythonselenium\\MTN\\Features\\Resources\\static content\\file_example_MP3_700KB.mp3"
dir_path = Path(__file__).parent
filePath_audioFile_var = os.path.join(dir_path, "static content/file_example_MP3_700KB.mp3")
popUp_msg_save_successful = (By.ID,"msgTemporary")

############ GroupLevel Locators HOME PAGE ##############################
link_users_page = (By.CSS_SELECTOR,".users > a")
contactList_tile = (By.CSS_SELECTOR, ".aPhonebook .label")
pickUpGroups_tile = (By.CSS_SELECTOR, ".aPickupGroups > .wrap")
huntGroups_tile = (By.CSS_SELECTOR, ".aHuntGroups .label")
openingHoursAndClosingDays_tile = (By.CSS_SELECTOR, ".aOpeningHours .label")
musicOnHold_tile = (By.CSS_SELECTOR, ".aMusicOnHold .label")
callQueues_tile = (By.CSS_SELECTOR, ".aCallQueues .label")
autoAttendants_page = (By.CSS_SELECTOR, ".aAutoAttendants span")
companyAnnouncements_tile = (By.CSS_SELECTOR, ".aAnnouncements .label")
myDevices_tile = (By.CSS_SELECTOR, ".aDevices .label")
signOff_tile = (By.CSS_SELECTOR, ".aLogout > .wrap")
myGroup_nav = (By.CSS_SELECTOR, ".group span")
myGroup_edit_admin = (By.XPATH, "//a[contains(text(),'Edit')]")
myGroup_firstName_edit = (By.CSS_SELECTOR, "#inpFirstName")
myGroup_lastName_edit = (By.CSS_SELECTOR, "#inpLastName")
myGroup_language_edit = (By.XPATH, "//label[contains(.,'English')]")
myGroup_edit_password = (By.XPATH, "//a[contains(.,'Change password')]")
myGroup_admin_oldpw = (By.CSS_SELECTOR, "#cInpOldPassword")
myGroup_admin_pw = (By.CSS_SELECTOR, "#cInpPassword")
myGroup_admin_repeatpw = (By.CSS_SELECTOR, "#cInpRepeat")
tileList_HomePage = (By.CSS_SELECTOR, "#page > main > section > div > ul")
link_home_page = (By.CSS_SELECTOR, ".home > a")



# PHONEBOOK PAGE
addContact_button = (By.CSS_SELECTOR, ".lnkAddContact > a")
addNewContactForm_firstName = (By.ID,"inpUserFirstName")
addNewContactForm_lastName = (By.ID,"inpUserLastName")
addNewContactForm_contactNr = (By.ID,"inpUserNumber")
addNewContactForm_save = (By.CSS_SELECTOR,".actions button")
ContactForm_delete = (By.CSS_SELECTOR,".lnkDelete > a")
contactForm_error = (By.CSS_SELECTOR,"#ovrContact div.error")
contactForm_confirm_delete = (By.CSS_SELECTOR,"#ovrContactDelete button")


# PICK UP GROUPS PAGE
addNewPickupGroup_button = (By.CSS_SELECTOR,".lnkAddpickupgroup > a")
pickUpGroup_name_field = (By.ID,"inpUserFirstName")
saveBtn_addPickUpGroup_form=(By.XPATH,"//button[@type='submit']")
error_pickUpGrp_nameField = (By.CSS_SELECTOR,".error:nth-child(3)")
editPickUpGroup_link = (By.XPATH,"//section[@id='tab-Netaxis PG']/div/div[2]/a")
deletePickUpGroup_link = (By.XPATH,"//*[@id='tab-Netaxis PG_edited']/div/div[3]/a")
confirm_deletePickUpGroup_button = (By.CSS_SELECTOR,"#ovrPickupGroupsDelete  button")
newPickupGroup = (By.XPATH, "//h3[contains(.,'Netaxis PG')]")
editedPickupGroup = (By.XPATH, "//h3[contains(.,'Netaxis PG_edited')]")
edit_pg_name = (By.CSS_SELECTOR,"#inpUserFirstName")
edit_pg_member = (By.CSS_SELECTOR,"label[for='inpSecondCalls-0']")
edit_pg_save = (By.CSS_SELECTOR,"#ovrPickupGroups div.btnSubmit > button")
edited_pg_name = (By.XPATH,"//*[@id='tab-Netaxis PG_edited']")
edited_pg_member = (By.CSS_SELECTOR,".modMember")

# HUNT GROUPS PAGE
addNewHuntGroup_button =(By.XPATH,"//a[contains(.,'Add hunt group')]")
huntGroup_add_name_field = (By.XPATH, "//*[@id='inpHuntGroupName']")
huntGroup_name_field = (By.CSS_SELECTOR, "#inpHuntgroupName")
# huntGroup_name_field =(By.ID,"inpExtension")
addHgForm_next = (By.PARTIAL_LINK_TEXT,"Next")
addHgForm_save = (By.PARTIAL_LINK_TEXT,"Save")
#addHgForm_delete = (By.XPATH,"//div/div/div[2]/a")
delete_hunt_group = (By.XPATH, "//section[@id='tab-Wouters updated Hunt group']/div/div/div[2]/a")
addHgForm_confirm_delete = (By.CSS_SELECTOR,"#ovrConfirm button")
editBtn_huntGroupName = (By.XPATH,"//div/h2/div/a")
addHgForm_user = (By.XPATH, "//label[contains(.,'testuser testuser')]")
save_edited_huntGroupName = (By.CSS_SELECTOR,"#ovrUpdateHuntgroupName button")
activate_huntGroup_toggle = (By.CSS_SELECTOR,".option > .control label")
manage_huntGroup_members_link=(By.CSS_SELECTOR,".subtitle a")
save_edited_huntGroupMembers = (By.CSS_SELECTOR,"#ovrUpdateHuntgroupUsers button")
huntGroup_added_check_edited = (By.XPATH, "//section[@id='tab-Wouters updated Hunt group']/header/h3/a")
huntGroup_added_check = (By.XPATH, "//section[@id='tab-test_hunter']/header/h3/a")
edit_hunt_name = (By.XPATH, "//a[contains(.,'Edit name')]")
edit_manage_members = (By.XPATH, "//a[contains(.,'Manage members')]")
#edit_phone_number = (By.CSS_SELECTOR, "a[href='#tab-phonenumber0']")
edit_phone_number = (By.CSS_SELECTOR, "#tab-phonenumber0 .heading > a")

edit_policy = (By.CSS_SELECTOR, "#tab-policy0 .selection")
edit_skip_to_next_member = (By.CSS_SELECTOR, "#tab-skipmember0 .heading > a")
edit_forward_after = (By.CSS_SELECTOR, "#tab-forwardafter0 .heading > a")
edit_forward_to = (By.CSS_SELECTOR, "#tab-forwardto0 .heading > a")
edit_hunt_user1 = (By.XPATH, "/html/body/div/div/div/div/div[2]/section[5]/div/div/div/div[2]/div/div[1]/div")
# edit_hunt_user1 = (By.CSS_SELECTOR, "label[for='inpUserSelect-0']")
edit_hunt_user2 = (By.CSS_SELECTOR, "#ovrUpdateHuntgroupUsers .control.jActive label")
edit_hunt_group_extension =  (By.CSS_SELECTOR, "#inpExtension_0")
save_hunt_group_extension = (By.XPATH, "//a[contains(.,'Save extension')]")
#hunt_group_policy_selector = (By.CSS_SELECTOR, "#tab-policy0 div:nth-child(4) > .control")
hunt_group_policy_selector = (By.XPATH, "//label[contains(.,'Uniform')]")
hunt_group_member_skip = (By.CSS_SELECTOR, "#tab-skipmember0 div:nth-child(6) label")
hunt_group_forward_after_selector = (By.XPATH, "//label[contains(.,'1 minute')]")
hunt_group_forward_to_wouter = (By.XPATH, "//h3[contains(.,'Wouter Goris')]")
hunt_group_submit_forward_to = (By.CSS_SELECTOR, "#cOvrForwardNumberSelect div.btnSubmit > button")
hunt_group_phone_number = (By.XPATH, "//label[contains(.,'+27472188833')]")
hunt_group_change_forward_to = (By.XPATH, "//a[contains(.,'Change')]")
hunt_group_phonebook = (By.XPATH, "//a[contains(@href, '#tab-user-2')]")
#hunt_group_phone_number = (By.CSS_SELECTOR, "#inpUserSelect-2")






# OPENING HOURS AND CLOSING DAYS PAGE
closingDays_tab = (By.CSS_SELECTOR, "#tab-closing-days > header .selection")
# openingHours_tab = (By.CSS_SELECTOR, "#tab-opening-hours > header .selection")
importOfficial_bankHolidays_link = (By.CSS_SELECTOR, ".lnkImport > a")
importButton_OfficialBankHolidays = (By.CSS_SELECTOR, "footer > .btnSubmit > button")
openingHours_tab = (By.CSS_SELECTOR, "#tab-opening-hours .heading > a")
manageOpeningHours_link = (By.XPATH, "//section[@id='tab-opening-hours']/div/section/footer/div/div/a")
manageOpeningHours_form = (By.XPATH, "//*[@id='ovrScheduleManage' and contains(@class,'overlay pageOverlay jActive')]")
closedOpen_toggle=(By.CSS_SELECTOR, ".spec:nth-child(1) .option > .control label")
copyOpeningHours_monday=(By.CSS_SELECTOR, "spec:nth-child(1) .lnkSimple:nth-child(2) > a")

hours_monday_toggle = (By.CSS_SELECTOR, "label[for='inpToggleMonday']")
hours_monday_addTimeSlot = (By.CSS_SELECTOR, "#ovrScheduleManage div.lnkSimple.lnkAddSlot > a")
hours_monday_addTimeFrom = (By.CSS_SELECTOR, "#inpSlotFrom-0-0")
hours_monday_addTimeTo = (By.CSS_SELECTOR, "#inpSlotTo-0-0")
hours_change_form_save = (By.CSS_SELECTOR, "#ovrScheduleManage div.btnSubmit > button")
monday_time_changes = (By.XPATH, "//*[@id='tab-opening-hours']/div/section/div/div/div[1]/div[3]/div/div[2][contains(.,'From 01:00 to 02:00')]")
# MUSIC ON HOLD PAGE

# CALL QUEUES PAGE
add_newCallQue = (By.CSS_SELECTOR, ".lnkAdd > a")
add_newDevice = (By.CSS_SELECTOR, ".lnkAdd > a")
call_queue_details= (By.CSS_SELECTOR, "")



# ADD NEW CALL QUEUE
callQueue_delete_confirm = (By.CSS_SELECTOR, "#ovrConfirm button[type='submit']")
callQueue_delete = (By.CSS_SELECTOR, "#tab-testQueue .delete > a")
callQueue_edit_details = (By.CSS_SELECTOR, "#ovrQueueDetails.jActive")
callQueue_edit = (By.CSS_SELECTOR, "#tab-testQueue .lnkEdit > a")
callQueue_details = (By.CSS_SELECTOR, '#tab-testQueue > header .selection')
callQueue_error = (By.CSS_SELECTOR, ".error")
addForm_queueName = (By.CSS_SELECTOR, "#inpName")
addForm_extension = (By.CSS_SELECTOR, "#inpExtension")
addForm_basic = (By.CSS_SELECTOR, "label[for='inpType-0']")
addForm_standard = (By.CSS_SELECTOR, "label[for='inpType-1']")
addForm_premium = (By.CSS_SELECTOR, "label[for='inpType-2']")
addForm_firstName = (By.CSS_SELECTOR, "#inpFirstname")
addForm_lastName = (By.CSS_SELECTOR, "#inpLastname")
addForm_password = (By.CSS_SELECTOR, "#inpPassword")
addForm_langENG = (By.CSS_SELECTOR, 'label[for="inpLanguage-2"')
addForm_langFR = (By.CSS_SELECTOR, 'label[for="inpLanguage-1"')
addForm_langNL = (By.CSS_SELECTOR, 'label[for="inpLanguage-0"')
form_submit = (By.CSS_SELECTOR, "button[type='submit']") 
callQueue_finder = (By.CSS_SELECTOR,"#tab-testQueue")
#callQueue = (By.CSS_SELECTOR,"#tab-"+TestDataMtnScp.mtn_scp_testCallqueue.name)

####### ADD NEW AA LOCATORS #########
addNewAA = (By.XPATH, "//a[contains(.,'Add auto attendant')]")
aa_name_field = (By.CSS_SELECTOR, ".control > #inpIvrName")
aa_extension_field = (By.ID, "inpExtension")
aa_telNr = (By.XPATH, "//div[5]/div[2]/div/div/div")
aa_type_select = (By.CSS_SELECTOR, "label[for='inpIvrType-0'")
aa_list = (By.CSS_SELECTOR, ".pageMain")
new_aa = (By.XPATH, "//h3[contains(.,'NetaxisAA')]")
new_aa_change = (By.XPATH, "//section[contains(@id,'tab-NetaxisAA')]//div[contains(@class,'lnkEdit')]")
edit_aa_extention = (By.CSS_SELECTOR, "#inpExtension0")
new_aa_delete = (By.XPATH, "//*[@id='tab-NetaxisAA']//div[contains(@class,'lnkDelete')]/a")
saveBtn_add_new_aa_form = (By.CSS_SELECTOR, "#ovrIvrAdd button")
saveBtn_edit_aa_form = (By.CSS_SELECTOR, "#ovrIvrNumber button")
error_aa_message = (By.CSS_SELECTOR, "#ovrAddHuntGroup .error")
closeForm = (By.PARTIAL_LINK_TEXT, "Close")
check_extention_20202020 = (By.XPATH, "//*[@id='tab-NetaxisAA']//div[contains(.,'2020') and contains(@class,'value')]")


# 'MessageReturnToTenant' :context.driver.find_element(By.ID, "msgReseller"),
# 'usersOverView' : context.driver.find_element(By.XPATH, "//nav[@id='nav']/ul/li[2]/a"),
continueAsEndUserLink = (By.CSS_SELECTOR, ".jActive .lnkSimple:nth-child(6) > a")


######### USERS PAGE####################################
upperMostUserTab = (By.CSS_SELECTOR, ".userdView:nth-child(1) a > .heading")
saNetaxisUser = (By.XPATH, "//div[@class='userdView' and contains(.,'sa Netaxis')]")

# ADD USER TO GROUP
addUserBtn = (By.CSS_SELECTOR, ".lnkAdd > a")

addUserForm_userId_field = (By.CSS_SELECTOR, ".control > #inpUserId")
addUserForm_password_field = (By.CSS_SELECTOR, ".control > #inpPassword")
addUserForm_passwordRepeat_field = (By.CSS_SELECTOR, ".control > #inpPasswordRepeat")
addUserForm_firstName_field = (By.CSS_SELECTOR, ".control > #inpUserFirstName")
addUserForm_lastName_field = (By.CSS_SELECTOR, ".control > #inpUserLastName")
addUserForm_email_field = (By.CSS_SELECTOR, ".control > #inpUserEmailAddress")

addUserForm_nextBtn = (By.PARTIAL_LINK_TEXT, "Next")

#languageList = ".sectionedContent:nth-child(1) #tab-language-1 .spec radio regular checkList > .option"
languageList = ".sectionedContent:nth-child(1) .option"

languageEnglish = (By.CSS_SELECTOR, ".sectionedContent:nth-child(1) div.option div:nth-child(1) .control")

saveUserAndSkipNextStepsBtn = (By.PARTIAL_LINK_TEXT, "Save user and skip next steps")

deleteUserBtn = (By.CSS_SELECTOR, ".jActive .lnkSimple:nth-child(5) > a")
confirmDeleteUserBtn = (By.CSS_SELECTOR, "#ovrConfirm .actions button")

# EDIT USER
editPersonalDetailsBtn = (By.CSS_SELECTOR, ".jActive .fiche:nth-child(1) a")
firstNameEditField = (By.CSS_SELECTOR, ".control > #inpUserFirstNameDetails")
lastNameEditField = (By.CSS_SELECTOR, ".control > #inpUserLastNameDetails")
saveBtnEditUserDetailsForm = (By.CSS_SELECTOR, "#ovrUserDetailsEdit button")
editUserServicesBtn = (By.CSS_SELECTOR, ".jActive .fiche:nth-child(4) a")


saveBtnEditUserServicesForm = (By.CSS_SELECTOR, "#ovrUserServicesEdit button")
emailEditField = (By.CSS_SELECTOR, ".control > #inpUserEmailAddressDetails")
pwEditField = (By.CSS_SELECTOR, ".control >  #inpUserPassword")
language_list_edit_userdetails = ".main > .radio:nth-child(1) .option"

editUserNumberBtn = (By.CSS_SELECTOR, ".jActive .fiche:nth-child(2) a")
extensionEditField = (By.CSS_SELECTOR, ".control > #inpUserExtension")
editNrIncoming = (By.CSS_SELECTOR, "#tab-phoneNumber-1 .heading > a")
number_list_incoming="#tab-phoneNumber-1 .option"
editNrOutgoing = (By.CSS_SELECTOR, "#tab-phoneNumber-2 .heading > a")
number_list_outgoing="#tab-phoneNumber-2 .option"
saveBtnEditUserNumberForm = (By.CSS_SELECTOR, "#ovrUserNumberEdit button")

editUsersDeviceBtn = (By.CSS_SELECTOR, ".jActive .fiche:nth-child(3) a")

changeUsersDeviceTo = (By.CSS_SELECTOR, ".step > .spec div:nth-child(3) label")
nextBtn_EditUserDeviceForm = (By.CSS_SELECTOR, "#ovrUserDeviceEdit .lnkNext > a")
MAC_addressField = (By.ID, "inpDeviceMacAddress")
saveBtnEditUserDeviceForm = (By.CSS_SELECTOR, ".lnkSave > a")


# COMPANY ANNOUNCEMENTS PAGE
addNewAnnouncementBtn = (By.XPATH, "//section[@id='page']/main/div/div/a")
groupAnnouncementName_field = (By.ID,"inpAnnouncementName")
fileToBeUploaded = (By.CSS_SELECTOR ,"#inpAnnouncementFile")
select_chooseMessage = (By.CSS_SELECTOR, "div:nth-child(1) > .control label")
select_recordMessage = (By.CSS_SELECTOR, "div:nth-child(3) > .control label")
start_and_stop_recording_btn = (By.CSS_SELECTOR,".lnkRecord > a")
stopRecording=(By.XPATH,"//a[contains(.,'Stop recording')]")
edit_companyAnnouncement_icon =(By.CSS_SELECTOR, "footer > .lnkEdit > a")
saveBtn_on_uploadAnnouncement_form = (By.CSS_SELECTOR, "#ovrAnnouncementUpload button")
saveBtn_on_recordAnnouncement_form = (By.CSS_SELECTOR,"#ovrAnnouncementRecord .actions button")
saveBtn_on_generalAnnouncement_form = (By.CSS_SELECTOR, "#ovrAnnouncement button")

deleteBtn_groupAnnouncement =(By.XPATH, "//a[contains(.,'Delete announcement')]") 
confirm_deleteBtn_groupAnnouncement =(By.CSS_SELECTOR, "footer > .btnSubmit > button") 

# MY DEVICES PAGE
save_edit_devices = (By.CSS_SELECTOR, "#ovrPickupGroups .btnSubmit button[type='submit']")
edit_device = (By.XPATH, "//h3[contains(., 'test_station_name')]//..//..//..//footer/div[1]/a")
delete_device = (By.XPATH, "//h3[contains(., 'test_station_name')]//..//..//..//footer/div[2]/a")
delete_device_existing_user = (By.XPATH, "//h3[contains(., 'device testuser testuser')]//..//..//..//footer/div[2]/a")
confirm_delete_device = (By.CSS_SELECTOR, "#ovrDeviceDelete button[type='submit']")
device_finder = (By.XPATH, "//h3[contains(., 'test_station_name')]")
error_device_in_use = (By.CSS_SELECTOR, "#msgError")
# ADD NEW DEVICE
addForm_DeviceType = (By.CSS_SELECTOR, "label[for='inpPhoneTypes-0']")
addForm_baseStationName =(By.CSS_SELECTOR, "#inpBaseStationName")
addForm_MacAdress = (By.CSS_SELECTOR, "#inpBaseStationMacAddress")

# ENDUSER PAGE   Dubplicate un user selectors !! 
saNetaxisPortalPage = (By.XPATH, "//h2[@class='heading' and contains(.,'sa Netaxis')]")
callHistoryTile = (By.XPATH, "//ul/li/a/div[@class='label' and contains(.,'Call history')]")
phoneBookTile = (By.XPATH, "//ul/li/a/div[@class='label' and contains(.,'Phone book')]")
voicemailTile = (By.XPATH, "//ul/li/a/div[@class='label' and contains(.,'Voicemail')]")
forwardCallsTile = (By.XPATH, "//ul/li/a/div[@class='label' and contains(.,'Forward all calls')]")
myCallCenterTile = (By.XPATH, "//ul/li/a/div[@class='label' and contains(.,'My call centers')]")
doNotDisturbTile = (By.XPATH, "//ul/li/a/div[@class='label' and contains(.,'Do not disturb')]")
returnGroupAdmin = (By.CSS_SELECTOR, "#msgAdmin")

# SERVICES
advancedService = (By.CSS_SELECTOR, ".spec.checkbox.regular.checkList .value .option .control")
saveBtn = (By.CSS_SELECTOR, "#ovrUserServicesEdit .btnSubmit button[type='submit']")
confirmDelUserBtn = (By.CSS_SELECTOR, "#ovrConfirm  .btnSubmit button[type='submit']")
