import time
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Features.Resources.BasePage import *
from Features.Resources.TenantLevelLocators import *

#######################################

#context.driver.execute_script("arguments[0].click();", confirmDeleteGroupBtn)


#######################################
@Given ("I am on the home page on tenant level")
def on_tenant_level(context):
    try:
       is_visible(context,addGroupButton)
    except TimeoutException:
        click(context,homePage_tenantLvl)


@When ("I click the add group button")
def click_addGroup_btn(context):
    click(context, addGroupButton)

@When ("I provide all the required data")
def provide_required_data(context):
    enter_text(context, addGroupForm_groupName_field,"TestGroup2020")
    enter_text(context, addGroupForm_contactName_field,"TestGroupContact")
    enter_text(context, addGroupForm_phoneNumber_field,"0472188837")
    enter_text(context, addGroupForm_email_field,"test@gmail.com")
    enter_text(context, addGroupForm_street_field,"troonstraat 60")
    enter_text(context, addGroupForm_postalCode_field,"1000")
    enter_text(context, addGroupForm_city_field,"Brussels")
    enter_text(context, addGroupForm_country_field,"Belgium")


@When ("I click the save button")
def save_group(context):
    click(context, addGroupForm_saveButton)

@Then ("A new group should be added to the tenant")
def check_if_groupWasAdded(context):
    time.sleep(10)
    NewlyAddedGroupName = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.CSS_SELECTOR, ".pageHeader > .heading"))
    time.sleep(5)
    assert NewlyAddedGroupName.text == "TESTGROUP2020"
    



########### EDIT GROUP

@When ("I navigate to the details overview of the group I want to edit and click 'edit'")
def select_group_to_be_edited(context):
    groupToBeEdited = (By.XPATH,"//h3[starts-with(.,'TestGroup2020')]")
    click(context, groupToBeEdited)
    click(context, editBtn_editGroupForm)

@When ("I change the group details and click save")
def change_group_details(context):
    clear_text_field(context,addGroupForm_groupName_field)
    enter_text(context, addGroupForm_groupName_field,"EditedGroup2020")
    clear_text_field(context,addGroupForm_contactName_field)
    enter_text(context, addGroupForm_contactName_field,"EditedGroupContact")
    clear_text_field(context,addGroupForm_phoneNumber_field)
    enter_text(context, addGroupForm_phoneNumber_field,"0471202004")
    clear_text_field(context,addGroupForm_email_field)
    enter_text(context, addGroupForm_email_field,"edited@gmail.com")
    clear_text_field(context,addGroupForm_street_field)
    enter_text(context, addGroupForm_street_field,"changedstreet 60")
    clear_text_field(context,addGroupForm_postalCode_field)
    enter_text(context, addGroupForm_postalCode_field,"1000")
    clear_text_field(context,addGroupForm_city_field)
    enter_text(context, addGroupForm_city_field,"Amsterdam ")
    clear_text_field(context,addGroupForm_country_field)
    enter_text(context, addGroupForm_country_field,"The Netherlands")

    saveBtn_editGroupForm=(By.XPATH,"//button[contains(.,'Save')]")
    click(context,saveBtn_editGroupForm)

@Then ("the new group details should saved")
def check_if_the_new_details_were_saved(context):
    is_visible(context,popUp_msg_save_successful)

####### DELETE GROUP
@When ("I navigate to the details overview of the group I want to delete")
def select_group(context):
    groupToBeDeleted = (By.XPATH,"//h3[starts-with(.,'EditedGroup2020')]")
    click(context, groupToBeDeleted)

@When ("I Click 'delete group' and confirm the delete action")
def delete_group_from_tenant(context):
    click(context, deleteGroupBtn)
    click(context,confirmDeleteGroupBtn)
    time.sleep(5)

@Then ("the respective group should be deleted")
def check_if_group_is_not_avialable_anymore(context):
    groupToBeDeleted = (By.XPATH,"//h3[starts-with(.,'EditedGroup2020')]")
    is_invisible(context, groupToBeDeleted)
    




