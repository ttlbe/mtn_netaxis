import time
from selenium import webdriver
from Features.TestDataMtnScp import user,TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import random
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *

##### create aa #####
@given('I navigate to the auto attendant page')
def navigate_to_auto_attendants(context):
    click(context, autoAttendants_page)

@when('I click \'add a new auto attendant\' button')
def click_add_new_auto_attendant(context):
    click(context, addNewAA)

@when('I provide all the required data an click save')
def provide_all_required_data(context):
    enter_text(context,aa_name_field,"NetaxisAA")
    click(context, aa_type_select)
    enter_text(context,aa_extension_field,"2020")
    click(context,saveBtn_add_new_aa_form)
      
@then('a new auto attendant should be added')
def check_if_aa_was_added_succesfully(context):
    assert(is_visible(context,new_aa))

##### provide wrong info #####
@When("I provide data which is not adhering to stipulation and click save")
def enter_wrong_existing_aa_data(context):
    enter_text(context,aa_name_field,"NetaxisAA")
    click(context, aa_type_select)
    enter_text(context,aa_extension_field,"2020")
    click(context,saveBtn_add_new_aa_form)

@Then("a clear error message should a appear and the AA should not get created")
def check_error_message(context):
    assert(is_visible(context,error_aa_message))

##### edit an existing AA #####
@When("I open the details tab of one of the available auto attendants")
def open_aa_tab(context):
    click(context,new_aa)

@When("I change the respective auto attendants data and click save")
def change_aa_data(context):
    click(context,new_aa_change)
    enter_text(context,edit_aa_extention,"2020")
    click(context,saveBtn_edit_aa_form)

@Then("the new settings should get saved")
def check_data_change(context):
    assert(is_visible(context,check_extention_20202020))

##### delete AA #####
@When("I click 'delete auto attendant' and confirm the delete action")
def delete_aa(context):
    click(context,new_aa_delete)

@Then("the respective auto attendant should be deleted")
def check_delete_aa(context):
    assert(is_visible(context,new_aa))
