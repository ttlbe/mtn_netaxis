import time
from Features.Resources.GroupLevelLocators import *
from Features.Resources.BasePage import *
from selenium.webdriver.common.by import By
from selenium import webdriver
import selenium

###Add a new call queue###
@Given("I navigate to the call queue page")
def nav_to_call_queue_page(context):
    click(context,callQueues_tile)

@When("I click 'add a new call queue'")
def click_add_new_call_queue(context):
    click(context,add_newCallQue)

@When("I provide all the required data and click 'save'")
def add_data_in_required_fields(context):
    enter_text(context,addForm_queueName,"testQueue")
    enter_text(context,addForm_extension,"7181")
    enter_text(context,addForm_firstName,"test Fname")
    enter_text(context,addForm_lastName,"test Lname")
    enter_text(context,addForm_password,"Test_123") #1 number 1# lowercase 1# uppdercase 1# special char 6-60chars long
    click(context,addForm_basic)
    click(context,addForm_langENG)
    click(context,form_submit)

@Then("a new call queue should be added")
def check_if_call_queue_is_added(context):
    assert(is_visible(context,callQueue_finder))

###provide data which is not adhering to stipulations during queue creation process###
@When("I provide data which is not respecting stipulations and click 'save'")
def add_incorrect_data(context):
    enter_text(context,addForm_queueName,"::::;;:;:;:;,;.$nD▲")
    enter_text(context,addForm_extension,";;;;;;;ž")
    click(context,addForm_basic)
    enter_text(context,addForm_firstName,"54545454Σ")
    enter_text(context,addForm_lastName,"5454545454▌")
    enter_text(context,addForm_password,"2313212é└")
    click(context,addForm_langENG)
    click(context,form_submit)

@Then("a clear error message should be provided and the queue should not be added")
def funcname(context):
    assert(is_visible(context,callQueue_error))

###edit an existing call queue###
@When("I open the details of an available call queue") 
def open_call_queue_details(context):
    click(context, callQueue_details)

@Then("I should be able to edit the queue details and options") 
def edit_call_queu_details(context):
    click(context, callQueue_edit)
    assert(is_visible(context, callQueue_edit_details))            

###delete a call queue###
@When("I click the delete icon and confirm the delete action")
def delete_call_queue(context):
    click(context, callQueue_delete)
    click(context, callQueue_delete_confirm)

@Then("the respective call queue should be deleted")
def check_if_call_queue_is_deleted(context):
    assert(is_invisible(context,callQueue_finder))