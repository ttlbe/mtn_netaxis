import time, selenium
from Features.Resources.TenantLevelLocators import *
from Features.Resources.BasePage import *
from Features.Resources.GeneralLocators import *
from selenium.webdriver.common.by import By
from selenium import webdriver
from Features.TestDataMtnScp import user, TestDataMtnScp, userData, groupData
from selenium.common.exceptions import NoSuchElementException

@given ("I am on the 'search' page")
def goto_search_page(context):
    click(context, searchPage_tenantLvl)

@when ("I provide the first name of one of the available end users")
def search_for_first_name(context):
    #enter first name is search field
    enter_text(context, firstName_field, context.TestDataMtnScp.userData_DONOTDELETE_ENDUSER.firstName)
    click(context, search_btn)
    #clear field for next step
    clear_text_field(context, firstName_field)

@then ("an overview of all the user that have the respective first name should be shown")
def check_users_table(context):
    is_visible(context, (By.XPATH, '//div[@class="tableContainer" and contains(.,'+context.TestDataMtnScp.userData_DONOTDELETE_ENDUSER.userId+')]'))
    

@when ("I provide the last name of one of the available end users")
def search_for_last_Name(context):
    #enter last name in search field
    enter_text(context, lastName_field, context.TestDataMtnScp.userData_DONOTDELETE_ENDUSER.lastName)
    click(context, search_btn)
    #clear field for next step
    clear_text_field(context, lastName_field)
    

@then ("an overview of all the user that have the respective last name should be shown")
def overview_last_Name(context):
    check_users_table(context)


@when ("I provide the phone number of one of the available end users")
def search_for_phone_number(context):
    enter_text(context, phoneNumber_field, context.TestDataMtnScp.userData_DONOTDELETE_ENDUSER.phoneNumber)
    click(context, search_btn)
    #clear field for next step
    clear_text_field(context, phoneNumber_field)

@then ("an overview of all the user that have the respective phone number should be shown")
def overview_phone_number(context):
    check_users_table(context)

@when ("I provide the email of one of the available end users")
def search_for_email(context):
    #enter email is search field
    enter_text(context, email_field, context.TestDataMtnScp.userData_DONOTDELETE_ENDUSER.eMail)
    click(context, search_btn)
    #clear field for next step
    clear_text_field(context, email_field)

@then("an overview of all the user that have the respective email should be shown")
def check_email(context):
    check_users_table(context)
    
@when ("I have searched a user")
def search_user(context):
    search_for_first_name(context)

@when ("I click the user ID")
def click_on_user(context):
    click(context, userId_table)


@then ("I should get redirected to the portal of the respective user")
def check_user_page(context):
    is_visible(context, (By.XPATH, '//section[@id="msgAdmin" and contains(.,'+context.TestDataMtnScp.userData_DONOTDELETE_ENDUSER.userId+')]'))
    #return to group lvl
    time.sleep(2)
    context.driver.find_element(By.XPATH, '//section[@id="msgAdmin" and contains(.,'+context.TestDataMtnScp.userData_DONOTDELETE_ENDUSER.userId+')]').click()
    #return to tenant admin lvl
    time.sleep(3)
    context.driver.find_element(By.XPATH, '//section[@id="msgReseller" and contains(.,'+context.TestDataMtnScp.groupData_grp_UNDELETEABLEGROUP_35647.groupName+')]').click()

@when ("I click the group ID")
def click_on_group(context):
    click(context, groupId_table)

@then ("I should get redirected to the portal of the group that contains the user I searched for")
def check_group_page(context):
    is_invisible(context, (By.XPATH, '//section[@id="msgReseller" and contains(.,'+context.TestDataMtnScp.groupData_grp_UNDELETEABLEGROUP_35647.groupName+')]'))
