import time
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Features.Resources.BasePage import *
from Features.Resources.TenantLevelLocators import *
from Features.Resources.GroupLevelLocators import *
from Features.Resources.EndUserLevelLocators import *
 
@given('I am on tenant level')
def step_impl(context):
    assert(is_visible(context,LicensesPage_tenantLvl))

@When ("I open the details tab for one of the available groups")
def open_group_details_tab(context):
    groupDetailsTab = (By.CSS_SELECTOR, ".modCustomer:nth-child(1) .heading") 
    click(context,groupDetailsTab)

@When ("I click the continue as group link")
def continue_to_groupLevel(context):
    click(context,continueAsGroupLink)

@Then ("I should be on the group admins portal")
def Verify_tenantAdmin_is_on_grpLvl(context):
    assert(is_invisible(context,openingHours_tab))


############################### CONTINUING FROM GROUP LEVEL TO END USER LEVEL #######################


@When ("I navigate to the users page")
def navigate_to_users_overView(context):
    click(context,link_users_page)

@When ( "I select one of the available users") 
def select_endUser(context):
    click(context,upperMostUserTab)

@when('I click the continue as end user link')
def continue_as_enduser(context):
    click(context,continueAsEndUserLink)

@Then ("I should be on the end users portal")
def check_if_element_is_present(context):
    assert(is_visible(context,eu_accountSettings_sideNav))
   