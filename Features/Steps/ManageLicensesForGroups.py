import time, selenium
from Features.Resources.GroupLevelLocators import *
from Features.Resources.BasePage import *
from Features.Resources.GeneralLocators import *
from selenium.webdriver.common.by import By
from selenium import webdriver
#from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException
from Features.Resources.TenantLevelLocators import *


@when("I click one of the available groups to access the respective groups details")
def click_on_group(context):
    click(context, un_del_group)

@when('I click on the licenses tab and click \'edit\'')
def step_impl(context):
    click(context, licences_header)
    click(context,edit_group_licences)

@when('I do not assign any of the available service packs to the respective group and click \'save\'')
def step_impl(context):

    ###  Click Array Hidden Checkboxes ### OPTION 1 TO FIX
    # licenseSlider = WebDriverWait(context.driver, 10).until(EC.visibility_of_all_elements_located((By.XPATH, '//div[@class="overlaycontent"]//div[@class="value"]/div/div/div/label')))
    # # licenseSlider = WebDriverWait(context.driver, 10).until(EC.visibility_of_all_elements_located((By.CSS_SELECTOR, ".serviceContainer div.checkbox div.input")))
    # # licenseTextField = context.driver.find_elements(By.CSS_SELECTOR, ".serviceContainer .spec.text input")   
    # click(context, (By.CSS_SELECTOR, 'div.overlaycontent'))
    # for i in range(get_list_length(context, licenseSlider)):
    #     e=licenseSlider[i]
    #     context.driver.execute_script("arguments[0].click();", e)
    #     # licenseTextField[i].clear()
    #     # licenseTextField[i].send_keys("0")
    #     i+=1
    
    ###  Click Array Hidden Checkboxes ### OPTION 2
    licenseSlider    = (By.XPATH, '//div[@class="overlaycontent"]//div[@class="value"]/div/div/div')
    for i in range(get_list_length(context, licenseSlider)):
        click_list(context, licenseSlider, i)
        licenseTextField = (By.CSS_SELECTOR, "#inpServicePack_"+i)
        clear_text_field(context, licenseTextField)
        enter_text(context, licenseTextField, "0")
        i+=1

    

@when(u'I clikc the \'continue as group\' link')
def step_impl(context):
    pass


@then(u'I should be redirected to the respective groups portal')
def step_impl(context):
    pass


@then(u'I should only see the following tile on home page .. # SKIP THIS STEP FOR NOW!')
def step_impl(context):
    pass