import time, random
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait,Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *

#### create pickup group ####
@Given ("I am on the pickup groups page")
def navigate_to_pickup_group_page(context):
    click(context,pickUpGroups_tile)

@When ("I click the 'add pickup group' button, provide all the required data and click 'save'")
def click_add_new_pickup_group(context):
    click(context,addNewPickupGroup_button)
    enter_text(context,pickUpGroup_name_field, "Netaxis PG")
    click(context,saveBtn_addPickUpGroup_form)
    
@Then ("a new pickup group should be added")
def check_if_pg_is_created_successfully(context):
    assert(is_visible(context, newPickupGroup))

#### edit a pickup group ####
@When("I open the details tab of one of the available pickup groups")
def ooen_pg_tab(context):
    click(context,newPickupGroup)

@When("edit the pickup group name, manage the members and change the configuration")
def edit_pg_settings(context):
    click(context,editPickUpGroup_link)
    enter_text(context,edit_pg_name,"_edited")
    click(context,edit_pg_member)
    click(context,edit_pg_save)

@Then("all the new settings should be set correctly")
def check_pg_changed_correctly(context):
    assert(is_visible(context,edited_pg_name))
    assert(is_visible(context,edited_pg_member))

#### delete a pickup group ####
@When("click 'delete pickup group'")
def click_and_confirm_delete_pg(context):
    click(context,deletePickUpGroup_link)
    click(context,confirm_deletePickUpGroup_button)

@Then("the respective pickup group should be deleted")
def check_pg_deleted(context):
    assert(is_invisible(context,editedPickupGroup))