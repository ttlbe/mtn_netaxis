import time
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *
from Features.Resources.GeneralLocators import *
from Features.Resources.EndUserLevelLocators import *

@given('I am on the end user outgoing call page')
def step_impl(context):
    click(context,eu_outgoingCalls_sideNav)


@when('I change the settings on end user outgoing call page')
def step_impl(context):
    click(context,eu_hideMyNr_toggle)


@then('All privacy settings should be saved')
def step_impl(context):
    assert(is_visible(context,eu_success))
    click(context,eu_hideMyNr_toggle)