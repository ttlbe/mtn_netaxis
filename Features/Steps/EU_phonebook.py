import time
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *
from Features.Resources.GeneralLocators import *
from Features.Resources.EndUserLevelLocators import *


@Given ("I am on the end user phonebook")
def navigate_to_endUser_phonebook(context):
    click(context,eu_phonebook_tile)

## See Manage Contacts
# @When ("I click add new company contact")
# @When ('I provide the "{firstname}" "{lastname}" "{phonenr}" and click save')
# @Then ('"{firstname}" "{lastname}" should be added to the contact list')
# @When("I provide illegal chars in the name fields and click 'save'")
# @Then("a clear error message should be provided and the contact shouldn't have been created")
# @When("I click the actions icon")
# @When("I click 'delete contact' and confirm the delete")
# @Then("the respective contact should be deleted")

