import time, selenium
from Features.Resources.GroupLevelLocators import *
from Features.Resources.BasePage import *
from selenium.webdriver.common.by import By
from selenium import webdriver
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException        

@Given("I navigate to 'my devices'")
def nav_to_devices(context):
    click(context, myDevices_tile)

@When("I click 'add a device'")
def click_add_new_device(context):
    click(context, add_newDevice)

@When("I provide all the required data and click save")
def add_data_in_required_fields(context):
    click(context, addForm_DeviceType)
    enter_text(context, addForm_baseStationName, "test_station_name")
    enter_text(context, addForm_MacAdress, "BBAADD449988")
    click(context, form_submit)

@Then("a new device should be added")
def check_if_device_is_added(context):  
    assert(is_visible(context,device_finder))


### Edit Devices  ###
@When("I click the edit icon of one of the available devices")
def click_edit_device(context):
    click(context,edit_device)

@When("I change the device settings and click 'save'")
def change_device_settings_and_save(context):
    enter_text(context,addForm_MacAdress, Keys.BACKSPACE + "7")
    click(context,save_edit_devices)

@Then("the changes should get saved")
def check_device_changes(context):
    assert(is_visible(context, (By.XPATH, "//div[contains(., 'BB:AA:DD:44:99:87')]")))



### Delete Device ###

@When("I click the delete icon of one of the available devices and confirm the delete action")
def delete_device_confirm(context):
    click(context,delete_device)
    click(context,confirm_delete_device)

@Then("the respective device should be deleted")
def assert_device_delete(context):
    assert(is_invisible(context,device_finder))

## delete assigned user device

@When("I click the delete icon of one of the devices that are allready in use")
def click_delete_device_in_use(context):
    click(context,delete_device_existing_user)
    click(context,confirm_delete_device)

@Then("a clear error message should pop up and the device should not get deleted")
def see_device_in_use_error(context):
    assert(is_visible(context,error_device_in_use))
