import time
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *
from Features.Resources.GeneralLocators import *
from Features.Resources.EndUserLevelLocators import *

@given('I am on the end user account settings page')
def step_impl(context):
    click(context,eu_accountSettings_sideNav)

@when('I change language settings')
def step_impl(context):
    click(context,eu_lang_fr)

@then('Language settings should be saved')
def step_impl(context):
    assert(is_visible(context,eu_success))
    click(context,eu_lang_eng)

@when('I change the password')
def step_impl(context):
    click(context,eu_change_password)
    enter_text(context,eu_old_password,"Netaxis123!!")
    enter_text(context,eu_new_password,"Netaxis123!!")
    enter_text(context,eu_confirm_new_password,"Netaxis123!!")
    click(context,eu_password_change_save)

@then('I should see a confirm popup')
def step_impl(context):
    assert(is_visible(context,eu_success))