import time
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *


#### Import the official bank holidays ####
@Given("I am on the 'opening hours and closing days' page")
def navigate_to_openinghours_and_closingdays(context):
    click(context, openingHoursAndClosingDays_tile)

@When ("I open the 'closing days' tab")
def open_closingDays_tab(context):
    click(context, closingDays_tab)

@When ("click 'import official bank holidays'")
def click_import_bankHolidays_link(context):
    click(context,importOfficial_bankHolidays_link)
    click(context,importButton_OfficialBankHolidays)

@Then ("all the official bank holidays should be imported and auto set as closing days")
def assert_that_official_bh_were_imported(context):
    assert(is_visible(context,popUp_msg_save_successful))

#### manage opening hours #### 
@When("I open the 'opening hours' tab")
def open_opening_hours_tab(context):
    click(context, openingHours_tab)

@When("click ' manage opening hours'")
def click_manage_opening_hours(context):
    click(context, manageOpeningHours_link)

@Then("I should be able to manage the opening hours")
def should_see_manage_form(context):
    assert(is_visible(context,manageOpeningHours_form))

#### change opening hours setting ####
@When("I change the opening hours")
def change_opening_hours(context):
    click(context,hours_monday_toggle)
    click(context,hours_monday_addTimeSlot)
    enter_text(context,hours_monday_addTimeFrom,"1")
    enter_text(context,hours_monday_addTimeTo,"2")
    click(context,hours_change_form_save)

@Then("I should be able to see changes")
def see_time_hours_changes(context):
    assert(is_visible(context,monday_time_changes))
