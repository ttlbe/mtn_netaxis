import time
import selenium
from selenium.webdriver.common.action_chains import ActionChains
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import selenium.webdriver.remote.file_detector
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *
    

@Given ("I navigate to the company announcements page")
def navigate_to_group_announcements_page(context):
    time.sleep(5)
    click(context,companyAnnouncements_tile)

@When ("I click the add new announcement button")
def click_add_new_announcement_button(context):
    time.sleep(5)
    click(context,addNewAnnouncementBtn)
    

@When("I provide a name")
def provide_group_announcement_name(context):
    enter_text(context,groupAnnouncementName_field,"netaxis test announcement")

@When ("I select 'choose message'")
def select_choose_message(context):
    click(context, select_chooseMessage)

@When ("select the file I want to upload")
def select_autdioFile_to_upload(context):
    enter_text(context, fileToBeUploaded, filePath_audioFile_var)

@When ("I click the save button on the add new announcement form")
def click_save_button_on_add_new_announcement_form(context):
    click(context, saveBtn_on_uploadAnnouncement_form)
    time.sleep(50)
    click(context, saveBtn_on_generalAnnouncement_form)
    time.sleep(50)

@Then ("A new company announcement should be added")
def verify_that_the_announcement_was_succesfully_added(context):
    newAnnouncement = (By.XPATH, "//h3[contains(.,'netaxis test announcement')]")
    is_visible(context, newAnnouncement)
         

   
################### check if the new announcement is playable ###########################################

    ### TO DO !!!!   STILL NEED FIGURE OUT HOW TO  KEEP
    ### TRACk OF PLAY BAR





######## THE ANNOUNCEMENT(S) THAT WHERE CREATED IN THE PREVIOUS STEP WILL BE EDITED
@When ("I click the edit icon of the group announcement I want to edit")
def click_edit_icon_group_announcement(context):
    #click(context,edit_companyAnnouncement_icon)

    cancelBtn = (By.PARTIAL_LINK_TEXT,"Cancel")
    temp_groupAnnouncementName_field = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.ID,"inpAnnouncementName"))

    i = 1
    while temp_groupAnnouncementName_field.get_attribute('value') != "netaxis test announcement":
        try:
            edit_icon = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.XPATH, "(//*[contains(@class, 'lnkSimple lnkEdit')])["+str(i)+"]")).click()
            time.sleep(3 )
            assert temp_groupAnnouncementName_field.get_attribute('value') == "netaxis test announcement"
        except AssertionError:    
            if temp_groupAnnouncementName_field.get_attribute('value') != "netaxis test announcement":
                print(temp_groupAnnouncementName_field.get_attribute('value'))
                click(context,cancelBtn)
                i= i+1
            elif temp_groupAnnouncementName_field.get_attribute('value') == "netaxis test announcement":    
                pass


@When ("I change the settings and click the save button")
def change_group_announcement(context):
    time.sleep(5)
    clear_text_field(context,groupAnnouncementName_field)
    enter_text(context, groupAnnouncementName_field, "edited")
    click(context, select_recordMessage)
    click(context, start_and_stop_recording_btn)
    time.sleep(5)
    is_visible(context,stopRecording)
    time.sleep(5)
    click(context, start_and_stop_recording_btn)

    click(context,saveBtn_on_recordAnnouncement_form)
    click(context,saveBtn_on_generalAnnouncement_form)


@Then ("the announcement settings should be changed")
def verify_that_the_announcement_was_succesfully_edited(context):
    editedAnnouncement = (By.XPATH, "//h3[contains(.,'edited')]")
    is_visible(context, editedAnnouncement)
        


######## THE ANNOUNCEMENT(S) THAT WHERE EDITED IN THE PREVIOUS STEP WILL BE DELETED

@When ("I click the delete icon of the group announcement I want to delete")
def delete_group_announcement(context):
    #click(context,deleteBtn_groupAnnouncement)  
    cancelBtn = (By.PARTIAL_LINK_TEXT,"Cancel")
    temp_groupAnnouncementName_field = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.ID,"inpAnnouncementName"))        
    

    i = 1
    while temp_groupAnnouncementName_field.get_attribute('value') != "edited":
        try:
            delete_icon = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.XPATH, "(//*[contains(@class, 'lnkSimple lnkDelete')])["+str(i)+"]")).click()
            time.sleep(5)
            assert temp_groupAnnouncementName_field.get_attribute('value') == "edited"
        except AssertionError:    
            if temp_groupAnnouncementName_field.get_attribute('value') != "edited":
                print(temp_groupAnnouncementName_field.get_attribute('value'))
                click(context,cancelBtn)
                i= i+1
            elif temp_groupAnnouncementName_field.get_attribute('value') == "edited":    
                pass



@When ("I confirm the delete operation")
def confirm_delete_group_announcement(context):
    click(context,confirm_deleteBtn_groupAnnouncement)

@Then ("the respective group announcement should be deleted")
def verify_that_announcement_is_deleted(context):
    editedAnnouncement = (By.XPATH, "//h3[contains(.,'abcdef')]")
    is_invisible(context, editedAnnouncement)
    

