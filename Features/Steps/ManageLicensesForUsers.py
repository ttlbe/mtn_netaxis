import time, selenium
from Features.Resources.GroupLevelLocators import *
from Features.Resources.BasePage import *
from Features.Resources.GeneralLocators import *
from selenium.webdriver.common.by import By
from selenium import webdriver
#from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException, TimeoutException


@Given ("I am on the home page on group level")
def on_group_level(context):
    try:
        is_visible(context, tileList_HomePage)
    except TimeoutException:
        click(context, link_home_page)

@when("I navigate to 'users'")
def goto_users_page(context):
    click(context, link_users_page)

@when("I click 'add user'")
def add_user(context):
    click(context, addUserBtn)

@when("I provide all the user details and click 'next'")
def fill_in_user_details(context):
    #fill out the new user
    enter_text(context, addUserForm_userId_field, "saNetaxis")
    enter_text(context, addUserForm_password_field, "saNetaxis456??")
    enter_text(context, addUserForm_passwordRepeat_field, "saNetaxis456??")
    enter_text(context, addUserForm_firstName_field, "sa")
    enter_text(context, addUserForm_lastName_field, "Netaxis")
    enter_text(context, addUserForm_email_field, "notarealemail@gmail.com")
    click(context, addUserForm_nextBtn)

@when("I select a language from the list and click 'save user and skip next steps'")
def save_user(context):
    click(context, languageEnglish)
    #time.sleep(1)
    #context.driver.find_element(By.PARTIAL_LINK_TEXT, "Save user and skip next steps").location_once_scrolled_into_view
    click(context, saveUserAndSkipNextStepsBtn)

@when("I click 'continue as end user..'")
def click_continue_as_end_user(context):
    #refresh page and continue to End user
    context.driver.refresh()
    click(context, saNetaxisUser)
    click(context, continueAsEndUserLink)

@then ("I should be redirected to the correct end-users portal")
def check_correct_portal(context):
    is_visible(context, saNetaxisPortalPage)

@then ("I should only see the 'call history' and 'phone book' tile")
def check_tiles(context):
    #check tiles
    tiles = context.driver.find_elements(By.CSS_SELECTOR, ".tileList .main ul li")
    assert len(tiles) == 2
    is_visible(context, callHistoryTile)
    is_visible(context, phoneBookTile)
    click(context, returnGroupAdmin)


@When ("I select the user I created without any licenses")
def select_user (context):
    click(context, saNetaxisUser)
    

@When ("I click 'edit' at the licenses section")
def edit_Services(context):
    time.sleep(1)
    click(context, editUserServicesBtn)



@When ("I select all the licenses and save")
def select_all_services(context):
    #select all licenses
    time.sleep(2)
    licenseList = context.driver.find_elements(By.CSS_SELECTOR, ".spec.checkbox.regular.checkList .value .option .control")
    for i in range(7):
        licenseList[i].click()
        i += 1
    click(context, saveBtn)


@Then ("I should see all the tiles that belong to the licenses")
def check_tiles_licenses(context):
    #check visible tiles
    is_visible(context, callHistoryTile)
    is_visible(context, phoneBookTile)
    is_visible(context, voicemailTile)
    is_visible(context, forwardCallsTile)
    is_visible(context, myCallCenterTile)
    is_visible(context, doNotDisturbTile)
    #delete created user after test
    click(context, returnGroupAdmin)
    click(context, saNetaxisUser)
    click(context, deleteUserBtn)
    click(context, confirmDelUserBtn)
    time.sleep(2)
    
    
