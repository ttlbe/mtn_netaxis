import time, random
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *
from selenium.webdriver.common.action_chains import ActionChains



  
#context.driver.execute_script("arguments[0].click();", confirmDeleteUserBtn)
random_extension = random.randint(1234,4321)

#################################################################################
@Given ("I click the users link")
def click_users_link(context):
    click(context,link_users_page)


@When ("I click the add user button")
def click_addUser_btn(context):
    click(context,addUserBtn)


@When ("I provide all the required user data")
def provide_required_userData(context):
    enter_text(context,addUserForm_userId_field, "TestUser2020")
    enter_text(context,addUserForm_password_field, "Netaxis123!!")
    enter_text(context,addUserForm_passwordRepeat_field,"Netaxis123!!")
    enter_text(context,addUserForm_firstName_field, "Netaxis")
    enter_text(context,addUserForm_lastName_field, "Solutions")
    enter_text(context,addUserForm_email_field, "netaxis@solutions.be")


@When ("I click next")
def click_next_btn(context):
    click(context, addUserForm_nextBtn)

@When ("I select a language")
def select_language(context):
    language_fr = (By.CSS_SELECTOR, ".sectionedContent:nth-child(1) div:nth-child(3) > .control")
    click(context, language_fr)

@When ("I skip the unnecessary steps and save the user")
def save_user_with_only_the_required_data(context):
    click(context,saveUserAndSkipNextStepsBtn)
    
@Then ("A new user should be added to the group")
def check_if_userWasAdded(context): 
    newUser = (By.XPATH, "//h3[contains(.,'Netaxis Solutions')]")
    is_visible(context,newUser)

       
########### try continue without providing the required data  during user creation process#################

@When ("I try to continue without providing the required data")
def continue_without_providing_data(context):
    click(context, addUserForm_nextBtn)

@Then ("a clear message should be provided under the required fields")
def check_message_under_required_fields(context):
    errorUserId = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.CSS_SELECTOR, "#inpUserId > .error"))
    errorPw = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.CSS_SELECTOR, "#inpPassword > .error"))
    errorFirstName = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.CSS_SELECTOR, "#inpUserFirstName > .error"))
    errorLastName = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.CSS_SELECTOR, "#inpUserLastName > .error"))
    errorEmail = WebDriverWait(context.driver, 10).until(lambda x: x.find_element(By.CSS_SELECTOR, "#inpUserEmailAddress > .error"))
    time.sleep(5)

    RequiredFields = [errorEmail,errorFirstName,errorLastName,errorUserId,errorPw]
    for item in RequiredFields:
        assert item.text == "Required field"

  
  
########### Edit the user that was created in previous scenario #####
@When ("I open the details tab of the user I want to edit")
def open_the_first_available_user(context):
    user_to_edit = (By.XPATH, "//h3[contains(.,'Netaxis Solutions')]")
    click(context,user_to_edit)
    time.sleep(5)


@When ("I edit the users personal details")
def edit_users_details(context):
    click(context, editPersonalDetailsBtn)
    clear_text_field(context, firstNameEditField)
    enter_text(context, firstNameEditField, "user")
    clear_text_field(context,lastNameEditField)
    enter_text(context, lastNameEditField, "isedited")
    clear_text_field(context,emailEditField)
    enter_text(context, emailEditField, "Netaxis@edited.be" )
    clear_text_field(context, pwEditField)
    enter_text(context,pwEditField, "ChangeMe!")
    c_select_random_element_from_list(context,language_list_edit_userdetails)
    click(context, saveBtnEditUserDetailsForm)
  

    
@When ("I edit the users number settings")
def edit_nr_settings(context):
    time.sleep(5)
    opened_user_tabs = (By.XPATH , "//*[contains(@class, 'jActive')]")
    click(context,opened_user_tabs)
    print("done broski")

            
    ############ edit the phone number for the user that was edited in the previous step
    user_to_edit=(By.XPATH, "//h3[contains(.,'user isedited')]")
    click(context,user_to_edit)
    time.sleep(10)
    click(context, editUserNumberBtn)
    clear_text_field(context, extensionEditField)
    enter_text(context, extensionEditField,str(random_extension))
    click(context,editNrIncoming)
    c_select_random_element_from_list(context,number_list_incoming)
    click(context, editNrOutgoing)
    c_select_random_element_from_list(context,number_list_outgoing)
    click(context, saveBtnEditUserNumberForm)
    
@When ("I edit the users device  settings")
def edit_device_settings(context):
    ############# edit the device settings
    time.sleep(10)
    click(context, editUsersDeviceBtn)
    click(context, changeUsersDeviceTo)
    click(context, nextBtn_EditUserDeviceForm)

    clear_text_field(context,MAC_addressField)
    enter_text(context,MAC_addressField,"ABCDEF202002")
    click(context, nextBtn_EditUserDeviceForm)
    click(context, saveBtnEditUserDeviceForm)
    time.sleep(10)
    
@When ("I edit the user services")
def edit_services(context):
    ############# edit the user services
    click(context,editUserServicesBtn)
    services_list_edit_user = ".checkbox .option"
 
    c_select_random_element_from_list(context,services_list_edit_user)
    c_select_random_element_from_list(context,services_list_edit_user)
    c_select_random_element_from_list(context,services_list_edit_user)
       
    click(context, saveBtnEditUserServicesForm)  
    

@Then ("the users details should be changed")
def check_if_new_values_are_shown_correctly(context):
    fname_field=context.driver.find_element(By.CSS_SELECTOR,".jActive .fiche:nth-child(1) .spec:nth-child(2) > .value")
    assert fname_field.text=="user"
    lname_field=context.driver.find_element(By.CSS_SELECTOR,".jActive .fiche:nth-child(1) .spec:nth-child(3) > .value")
    assert lname_field.text=="isedited"
    extension_field = context.driver.find_element(By.XPATH, "//div[contains(@class, 'value') and contains(.//., '"+str(random_extension)+"')]")
    assert extension_field.text == str(random_extension)




@When ("I select the user I want to delete")
def select_user_to_delete(context):
    user_to_delete=(By.XPATH, "//h3[contains(.,'user isedited')]")
    click(context,user_to_delete)
    
@When ("I click 'delete user' and confir the delete action")
def click_user(context):
    click(context,deleteUserBtn)
    click(context,confirmDeleteUserBtn)
    
@Then ("The respective user should be deleted")
def check_if_user_is_deleted(context):
    user_to_delete=(By.XPATH, "//h3[contains(.,'user isedited')]")
    is_invisible(context,user_to_delete )