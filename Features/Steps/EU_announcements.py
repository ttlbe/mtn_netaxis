import time
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *
from Features.Resources.GeneralLocators import *
from Features.Resources.EndUserLevelLocators import *

@given('I am on the end user announcements page')
def step_impl(context):
    click(context,eu_myAnnouncements_sideNav)

@when('I add an invalid announcement')
def step_impl(context):
    click(context,eu_add_new_announcement)
    enter_text(context,eu_add_anc_name,"invalid )╝.... &é'(§è!çà)test anc_2") ## broke system  no check at creation
    click(context,eu_save_anc)  

@then('I should see a clear error message')
def step_impl(context):
    assert(is_visible(context,eu_anc_error))


@when('I add a new announcement')
def step_impl(context):
    click(context,eu_add_new_announcement)
    enter_text(context,eu_add_anc_name,"test anc")
    click(context,eu_rec_message)
    click(context,eu_start_rec) #start
    time.sleep(2)               #record
    click(context,eu_start_rec) #stop
    click(context,eu_save_rec)  #save
    click(context,eu_save_anc) 

@then('I should see new annoucement')
def step_impl(context):
    assert(is_visible(context,eu_new_anc))


@when('I edit an announcement')
def step_impl(context):
    click(context,eu_edit_anc)
    enter_text(context,eu_add_anc_name,"_edited")
    click(context,eu_save_anc) 


@then('I should see the edited annoucement')
def step_impl(context):
    assert(is_visible(context,eu_edited_anc))

@when('I delete an announcement')
def step_impl(context):
    click(context,eu_delete_anc)
    click(context,eu_delete_anc_confirm)



@then('I should not see the annoucement')
def step_impl(context):
    assert(is_invisible(context,eu_edited_anc))