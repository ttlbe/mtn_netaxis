import time
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *
from Features.Resources.GeneralLocators import *


@Given ("I am on the contact list page")
def navigate_to_company_contact_list(context):
    click(context,contactList_tile)

@When ("I click add new company contact")
def click_add_new_company_contact(context):
    click(context, addContact_button)

@When ('I provide the "{firstname}" "{lastname}" "{phonenr}" and click save')
def provide_contacts_data(context, firstname,lastname,phonenr):
    enter_text(context, addNewContactForm_firstName, firstname)
    enter_text(context, addNewContactForm_lastName, lastname)
    enter_text(context, addNewContactForm_contactNr, phonenr)
    click(context, addNewContactForm_save)

@Then ('"{firstname}" "{lastname}" should be added to the contact list')
def check_if_contacts_are_added_successfully(context,firstname,lastname):
    newCc = (By.XPATH,"//h3[contains(.,'"+firstname+" "+lastname+"')]")
    is_visible(context, newCc)


#### add a new contact with illegal chars in the name fields ####
@When("I provide illegal chars in the name fields and click 'save'")
def funcname(context):
    enter_text(context, addNewContactForm_firstName, "/m")
    enter_text(context, addNewContactForm_lastName, "lastname")
    enter_text(context, addNewContactForm_contactNr, "0000 00 00 00")
    click(context, addNewContactForm_save)

@Then("a clear error message should be provided and the contact shouldn't have been created")
def funcname(context):
    assert(is_visible(context,contactForm_error))


#### delete a company contact #### !!! 
@When('I click the actions icon of "{firstname}" "{lastname}"')
def click_edit_contact(context,firstname,lastname):
    contactForm_edit_button = (By.XPATH, "//h3[contains(.,'"+firstname+" "+lastname+"')]/../..//footer/div[contains(@class,'lnkOptions')]/a")
    click(context,contactForm_edit_button)

@When("I click 'delete contact' and confirm the delete")
def click_delete_conf(context):
    click(context,ContactForm_delete)
    click(context,contactForm_confirm_delete)

@Then('contact "{firstname}" "{lastname}" should be deleted')
def check_delete_group_contact(context,firstname,lastname):
    deleted_Contact = (By.XPATH,"//h3[contains(.,'"+firstname+" "+lastname+"')]")
    assert(is_invisible(context,deleted_Contact))
