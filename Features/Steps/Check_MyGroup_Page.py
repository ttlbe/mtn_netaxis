import time, selenium
from Features.Resources.GroupLevelLocators import *
from Features.Resources.BasePage import *
from selenium.webdriver.common.by import By
from selenium import webdriver
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException        

@Given("I navigate to 'my group'")
def nav_to_my_group(context):
    click(context, myGroup_nav)

@When("I click 'edit' in the admin details section")
def click_edit_admin_details_button(context):
    click(context, myGroup_edit_admin)

@Then("I should be able to edit the name and language settings for the group admin")
def check_if_name_language_is_editable(context):
    assert(is_clickable(context, myGroup_firstName_edit))
    assert(is_clickable(context, myGroup_lastName_edit))
    assert(is_clickable(context, myGroup_language_edit))

@When("I click 'change password' in the admin details section")
def click_change_password(context):
    click(context, myGroup_edit_password)

@Then("I should be able to edit the group admins password")
def check_if_password_is_editable(context):
    assert(is_clickable(context, myGroup_admin_oldpw))
    assert(is_clickable(context, myGroup_admin_pw))
    assert(is_clickable(context, myGroup_admin_repeatpw))