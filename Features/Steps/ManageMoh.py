import time
from selenium import webdriver
from Features.TestDataMtnScp import user,TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *


## upload music file

@given('I Navigate to the music on hold page')
def navigate_to_moh(context):
    time.sleep(5)     #slow page
    moh_page = (By.XPATH,"//a[contains(.,'Music on hold')]")
    click(context,moh_page)

@when('I click \'change\'')
def click_change(context):
    divtwo = (By.CSS_SELECTOR, "div:nth-child(2) > .control label")
    change_btn = (By.XPATH, "//a[contains(.,'Change')]")
    click(context,divtwo)
    click(context,change_btn)
  

@when('I select upload a music file')
def select_upload_music_file(context):
    uploadmf=context.driver.find_element(By.CSS_SELECTOR, ".lnkAdd > a")
    uploadmf.click()
    
    fileName_field = context.driver.find_element(By.ID, "inpMusicName")
    fileName_field.send_keys("Nestaxis Music File")
    fileToBeUploaded = context.driver.find_element(By.ID ,"inpMusicFile")
    fileToBeUploaded.send_keys(filePath_audioFile_var)
    saveBtn_chooseMusic_form = (By.CSS_SELECTOR,"#ovrMusicUpload button")
    click(context,saveBtn_chooseMusic_form)
    #wait for upload
    upload = (By.CSS_SELECTOR, "div.cmpUploading")
    WebDriverWait(context.driver, 30).until(EC.invisibility_of_element(upload))
    


@then('the file i uploaded should be set for music on hold')
def check_if_uploaded_file_is_set_as_moh(context):
    moh = (By.XPATH," //label[contains(.,'Nestaxis Music File')]")
    is_visible(context,moh)

@then('the file i selected should be set for music on hold')
def check_if_uploaded_file_is_set_as_moh(context):
    moh = (By.XPATH," //label[contains(.,'test announcement')]")
    is_visible(context,moh)

@then(u'the library file i selected should be set for music on hold')
def check_if_uploaded_file_is_set_as_moh(context):

    is_visible(context,)
    raise NotImplementedError(u'STEP: the library file i selected should be set for music on hold')

@when(u'i select \'select from our library\'')
def step_impl(context):
    raise NotImplementedError(u'STEP: When i select \'select from our library\'')

@when('i select \'select a file that was uploaded previously\'')
def step_impl(context):
    open_upload_prev = (By.CSS_SELECTOR, "#ovrMusicChoice div:nth-child(3) > a")
    click(context,open_upload_prev)
    select_first_music = (By.CSS_SELECTOR, "label[for='inpWelcomeMessage-0']")
    click(context,select_first_music)
    clicksave_music = (By.CSS_SELECTOR, "#ovrMusicLibrary  div.btnSubmit > button")
    click(context,clicksave_music)
    