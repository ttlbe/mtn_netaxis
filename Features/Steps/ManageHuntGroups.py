import time, selenium
from Features.Resources.GroupLevelLocators import *
from Features.Resources.BasePage import *
from selenium.webdriver.common.by import By
from selenium import webdriver
from TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException
    

## add
@Given("I navigate to the hunt groups page")
def nav_to_hunt_groups(context):
    click(context, huntGroups_tile)

@When("I click 'add hunt group'")
def add_hunt_group(context):
    click(context, addNewHuntGroup_button)

@When("I provide all the required hunt data and click save")
def add_hunt_data(context):
    enter_text(context, huntGroup_add_name_field, "test_hunter")
    click(context, addHgForm_next)
    click(context, addHgForm_save)

@Then("a new hunt group should be added")
def check_if_hunt_group_is_added(context):
    assert(is_visible(context, huntGroup_added_check))

## add false
@When("I provide data which is not adhering to stipulations and click save")
def add_incorrect_data(context):
    enter_text(context, huntGroup_add_name_field, "///test///")
    click(context, addHgForm_next)

@Then("a clear error message should appear and the hunt group should not get created")
def check_if_error_is_displayed(context):
    assert(is_visible(context,error_aa_message))

## add details
@When("I open the details of one of the available hunt groups")
def click_edit_details_hunt_group(context):
    click(context, huntGroup_added_check)

@Then("possibilities should be provide to edit the hunt group name, manage the members and configuration settings")
def check_if_editable(context):
    assert(is_clickable(context, edit_hunt_name))
    assert(is_clickable(context, edit_manage_members))
    assert(is_clickable(context, edit_phone_number))
    assert(is_clickable(context, edit_policy))
    assert(is_clickable(context, edit_skip_to_next_member))
    assert(is_clickable(context, edit_forward_after))
    assert(is_clickable(context, edit_forward_to))

## delete 
##### setup should work waiting for add group functionality to work #####
@When("I click 'delete hunt group'")
def del_hunt_group(context):
    click(context, delete_hunt_group)
    click(context, addHgForm_confirm_delete)


@Then("the respective hunt group should get deleted")
def check_if_hunt_group_is_deleted(context):
   assert(is_invisible(context, huntGroup_added_check_edited))


@When("I open the details of the edited hunt group")
def click_edit_details_hunt_group(context):
    click(context, huntGroup_added_check_edited)

### edit and save and check
@When("I edit hunt group name, member and configuration settings and save")
def edit_hunt_group_details(context):
    click(context, edit_hunt_name)
    clear_text_field(context, huntGroup_name_field)
    enter_text(context, huntGroup_name_field, "Wouters updated Hunt group")
    click(context, save_edited_huntGroupName)

    click(context, edit_manage_members)
    #click(context, edit_hunt_user2)  ## deselect active user
    click(context, edit_hunt_user1)
    click(context, save_edited_huntGroupMembers)

    click(context, edit_phone_number)
    clear_text_field(context, edit_hunt_group_extension) ## contuniue 14/4
    enter_text(context, edit_hunt_group_extension, '8888')
    click(context, save_hunt_group_extension)

    time.sleep(5) ####### NEEDS TO CHANGE
    click(context, edit_phone_number)
    click(context, hunt_group_phone_number)

    time.sleep(5)
    click(context, edit_policy)
    click(context, hunt_group_policy_selector)

    time.sleep(5)
    click(context, edit_skip_to_next_member)
    click(context, hunt_group_member_skip)

    time.sleep(5)
    click(context, edit_forward_after)
    click(context, hunt_group_forward_after_selector)

    time.sleep(5)
    click(context, edit_forward_to)
    click(context, hunt_group_change_forward_to)
    click(context, hunt_group_phonebook)
    click(context, hunt_group_forward_to_wouter)
    click(context, hunt_group_submit_forward_to)
    
@Then("Hunt group should be updated accordingly")
def check_if_values_changed(context):
    click(context, edit_manage_members)
    assert(context.driver.find_element(By.CSS_SELECTOR, ".jActive #inpUserSelect-0"))
    context.driver.find_element(By.CSS_SELECTOR, "#ovrUpdateHuntgroupUsers header a").click()

    click(context, edit_phone_number)
    assert(context.driver.find_element(By.XPATH, '//*[@class="selection" and contains(., "+27472188833")]'))
    assert(context.driver.find_element(By.XPATH, "//*[@id='inpExtension_0']").get_attribute("value") == "8888")
    click(context, edit_phone_number)

    assert(context.driver.find_element(By.XPATH, '//*[@class="selection" and contains(., "Uniform")]'))
    assert(context.driver.find_element(By.XPATH, '//*[@class="selection" and contains(., "6")]'))
    assert(context.driver.find_element(By.XPATH, '//*[@class="selection" and contains(., "1 minute")]'))
    assert(context.driver.find_element(By.XPATH, '//*[@class="selection" and contains(., "+27478666837")]'))
