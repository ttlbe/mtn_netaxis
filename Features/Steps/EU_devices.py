import time
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *
from Features.Resources.GeneralLocators import *
from Features.Resources.EndUserLevelLocators import *

@given('I am on the end user device page')
def step_impl(context):
    click(context,eu_MyDevices_sideNav)


@when('I toggle \'Answer confirmation required\'')
def step_impl(context):
    click(context,eu_answerConfirm_toggle)


@then('Setting should be saved')
def step_impl(context):
    assert(context.driver.find_element(By.CSS_SELECTOR, "#inpAnswerConfirmation").is_selected())


@when('I add a new device with valid info')
def step_impl(context):
    click(context,eu_addPersonalDevice_btn)
    enter_text(context,eu_deviceNameField_personalDevice,"testDevice")
    enter_text(context,eu_phoneNrField_personalDevice,"0400112233")
    click(context,eu_saveBtn_addPersonalDevice)


@then('a new device should be saved')
def step_impl(context):
    assert(is_visible(context,eu_new_personalDevice))

@when('I add a new device with invalid info')
def step_impl(context):
    click(context,eu_addPersonalDevice_btn)
    enter_text(context,eu_deviceNameField_personalDevice,"/?-_%!§testDevice")
    enter_text(context,eu_phoneNrField_personalDevice,"0")
    click(context,eu_saveBtn_addPersonalDevice)

@then('a clear error message should appear')
def step_impl(context):
    assert(is_visible(context,eu_personalDevice_error))

@when('I edit and save device')
def step_impl(context):
    click(context,eu_new_personalDevice_edit)
    enter_text(context,eu_deviceNameField_personalDevice,"_edited")
    click(context,eu_saveBtn_addPersonalDevice)

@then('device should be edited and saved')
def step_impl(context):
    assert(is_visible(context,eu_new_personalDevice_edited))

@When('I delete a device')
def funcname(context):
    click(context,eu_new_personalDevice_edited_delete)


@Then('a device should be deleted')
def funcname(context):
    assert(is_invisible(context,eu_new_personalDevice_edited))

