import time
from selenium import webdriver
from Features.TestDataMtnScp import *
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from Features.Resources.BasePage import *
from Features.Resources.GroupLevelLocators import *
from Features.Resources.GeneralLocators import *
from Features.Resources.EndUserLevelLocators import *


@given('I am on the end user incoming call page')
def step_impl(context):
    click(context,eu_incomingCalls_sideNav)

@when('I change the settings on end user incoming call page')
def step_impl(context):
    click(context,eu_forwardAllCalls_tab)
    click(context,eu_forw_num)
    assert(is_visible(context,eu_success))
    click(context,eu_success)
    click(context,eu_forw_off)
    click(context,eu_success)


    click(context,eu_lineBusy_tab)
    click(context,eu_busy_num)
    # set_phone_number
    assert(is_visible(context,eu_success))
    click(context,eu_success)
    click(context,eu_busy_off)
    click(context,eu_success)


    click(context,eu_second_call)
    assert(is_visible(context,eu_success))
    click(context,eu_success)

    click(context,eu_callNotAns_tab)
    click(context,eu_noAns_num)
    # set_phone_number
    assert(is_visible(context,eu_success))
    click(context,eu_success)
    click(context,eu_noAns_off)
    click(context,eu_success)


    click(context,eu_timing_5)
    assert(is_visible(context,eu_active_timing_5))
    click(context,eu_timing_3)

    click(context,eu_whenDeviceNotReachable_tab)
    click(context,eu_notReachable_num)
    # set_phone_number
    assert(is_visible(context,eu_success))
    click(context,eu_success)
    click(context,eu_notReachable_off)
    click(context,eu_success)


    click(context,eu_voicemailsSettings_tab)
    click(context,eu_message_to_mail)
    assert(is_visible(context,eu_success))
    click(context,eu_success)
    click(context,eu_message_to_voice_inbox)
    click(context,eu_success)


    click(context,eu_copy_to_mail)
    assert(is_visible(context,eu_success))
    click(context,eu_success)
    click(context,eu_dont_Send_copy)
    click(context,eu_success)


    click(context,eu_rejectAnonymousCalls_toggle)
    assert(is_visible(context,eu_success))
    click(context,eu_success)
    click(context,eu_rejectAnonymousCalls_toggle)
    click(context,eu_success)

    
    click(context,eu_doNotDisturb_toggle)
    assert(is_visible(context,eu_success))
    click(context,eu_success)
    click(context,eu_doNotDisturb_toggle)
    click(context,eu_success)


@then('All settings should be saved')
def step_impl(context):
    pass


# def set_phone_number():
#     print("1")