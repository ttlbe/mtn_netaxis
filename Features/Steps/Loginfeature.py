import time
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException        
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Features.Resources.GeneralLocators import *
from Features.Resources.GroupLevelLocators import *
from Features.Resources.EndUserLevelLocators import *
from Features.Resources.BasePage import *


@Given("I am on the mtn scp login page")
def open_mtn_scp(context):
    context.driver.get(context.TestDataMtnScp.urlmtn_scp)

@When("I provide the correct credentials for tenant admin")
def mtn_enter_creds(context):
    enter_text(context,username_field,context.TestDataMtnScp.mtn_scp_tenantAdmin.login)
    enter_text(context, pw_field ,context.TestDataMtnScp.mtn_scp_tenantAdmin.password)    

@When("I click the submit button")
def mtn_click_submit(context):
    click(context,submitBtn)


@Then("I should be logged in")
def mtnScp_sleep(context):
    is_visible(context,addGroupButton)
    


#######################################  LOGIN AS A GROUP ADMIN     #######################################      


@When("I provide valid group admin credentials")
def mtn_enter_groupAdmin_creds(context):
    enter_text(context,username_field,context.TestDataMtnScp.mtn_scp_groupAdmin.login)
    enter_text(context,pw_field,context.TestDataMtnScp.mtn_scp_groupAdmin.password)    
    mtn_click_submit(context)


@Then("I should have access to the group admin portal")
def mtnScp_sleep(context):
    is_visible(context, openingHoursAndClosingDays_tile)
    


#######################################  LOGIN AS AN END USER   ####################################### 

@When("I provide valid end user credentials")
def mtn_enter_endUser_creds(context):
    enter_text(context,username_field,context.TestDataMtnScp.mtn_scp_endUser.login)
    enter_text(context,pw_field,context.TestDataMtnScp.mtn_scp_endUser.password)    
    mtn_click_submit(context)


@Then("I should have access to the end users portal")
def mtnScp_sleep(context):
    is_visible(context, eu_callHistory_tile)


