import time, selenium
from Features.Resources.TenantLevelLocators import *
from Features.Resources.BasePage import *
from Features.Resources.GeneralLocators import *
from selenium.webdriver.common.by import By
from selenium import webdriver
from Features.TestDataMtnScp import user, TestDataMtnScp
from selenium.common.exceptions import NoSuchElementException
import random
import string

@given ("I am on the 'settings' page")
def goto_settings_page(context):
    click(context, settingsPage_tenantLvl)

@when ("I click the 'edit' button in the administator details section")
def click_on_edit(context):
    click(context, editBtn_editAdministratorDetails)

@when ("I change the admins details and click save")
def change_admin_details(context):
    letter = string.ascii_lowercase
    context.randomLetters = ""
    for i in range(7):
        context.randomLetters += random.choice(letter)
    clear_text_field(context, administratorDetails_lastName_field)
    enter_text(context, administratorDetails_lastName_field, context.randomLetters)
    click(context, saveBtn_administratorDetails)

@then ("the new details should be saved")
def check_changed_details(context):
    is_visible(context, (By.XPATH, '//div[@class="value" and contains(.,'+context.randomLetters+')]'))


@when ("I provide the old password and the new password and click save")
def call_change_password(context):
    change_tenant_password(context, context.TestDataMtnScp.mtn_scp_tenantAdmin.password , "saNetaxis456??")

def change_tenant_password(context, oldPass, newPass):
    click(context, changePsw_administratorDetails)
    clear_text_field(context, administratorDetails_oldPsw_field)
    clear_text_field(context, administratorDetails_newPsw_field)
    clear_text_field(context, administratorDetails_repeatPsw_field)
    enter_text(context, administratorDetails_oldPsw_field, oldPass)
    enter_text(context, administratorDetails_newPsw_field, newPass)
    enter_text(context, administratorDetails_repeatPsw_field, newPass)
    click(context, saveBtn_administratorPassword)

@then ("the new password should be saved")
def check_saved_password(context):
    is_visible(context, editBtn_editAdministratorDetails)

@then ("I should be able to login with the new credentials")
def check_new_login(context):
    click(context, logOut_tenantLvl)
    enter_text(context,username_field, context.TestDataMtnScp.mtn_scp_tenantAdmin.login)
    enter_text(context, pw_field, "saNetaxis456??")    
    click(context,submitBtn)
    goto_settings_page(context)
    change_tenant_password(context, "saNetaxis456??", context.TestDataMtnScp.mtn_scp_tenantAdmin.password)


@when ("open the 'phone numbers' tab")
def open_phone_tab(context):
    click(context, phoneTab_phoneNumbers)

@then ("I should see an overview of all the available number ranges that were assigned to the respective tenant during provisioning")
def check_numbers_range(context):
    is_visible(context, rangesTable_phoneNumbers)