from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from Features.TestDataMtnScp import TestDataMtnScp
#from allure_behave.hooks import allure_report


#allure_report("C:\\Users\\azizb\\Desktop\\python\\pythonselenium\\MTN\\allure_result_folder")

def before_feature(context, feature):
    if 'tenant_level' in feature.tags:
        context.execute_steps('''
            Given I am on the mtn scp login page
             When I provide the correct credentials for tenant admin
             When I click the submit button
             Then I should be logged in
        ''')
    if 'group_level' in feature.tags:
          context.execute_steps('''
            Given I am on the mtn scp login page
             When I provide valid group admin credentials
             When I click the submit button
             Then I should have access to the group admin portal
          ''')
    if 'user_level' in feature.tags:
          context.execute_steps('''
            Given I am on the mtn scp login page
             When I provide valid end user credentials
             When I click the submit button
             Then I should have access to the end users portal
          ''')

def before_all(context):
	opts = webdriver.ChromeOptions()
	opts.add_argument('no-sandbox')
	opts.add_experimental_option("prefs", { 
    "profile.default_content_setting_values.media_stream_mic": 1, 
    "profile.default_content_setting_values.media_stream_camera": 1,
    "profile.default_content_setting_values.geolocation": 1, 
    "profile.default_content_setting_values.notifications": 1 
  })
	context.driver = webdriver.Chrome(chrome_options=opts)

	context.TestDataMtnScp = TestDataMtnScp()


def after_all(context):
	context.driver.quit()
	
	
