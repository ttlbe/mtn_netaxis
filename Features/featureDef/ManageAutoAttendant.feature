@group_level
Feature: Add a new auto attendant as group admin

        @addAA
        Scenario: succesfully add a new auto attendant
            Given I navigate to the auto attendant page
             When  I click 'add a new auto attendant' button
             When I provide all the required data an click save
             Then a new auto attendant should be added
        
        Scenario: add an auto attendant with data which is not adhering to stipulation
            Given I navigate to the auto attendant page
             When  I click 'add a new auto attendant' button
             When I provide data which is not adhering to stipulation and click save
             Then a clear error message should a appear and the AA should not get created

        @editAA
        Scenario: edit and existing auto attendant
            Given I navigate to the auto attendant page
             When I open the details tab of one of the available auto attendants
             When I change the respective auto attendants data and click save
             Then the new settings should get saved

        @delteAA
        Scenario: delete an auto attenadant
            Given I navigate to the auto attendant page
             When I open the details tab of one of the available auto attendants
             When I click 'delete auto attendant' and confirm the delete action
             Then the respective auto attendant should be deleted
