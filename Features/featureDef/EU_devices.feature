@user_level
Feature: device manage on end user level


        Scenario: edit Answer confirmation required Toggle
            Given I am on the end user device page
             When I toggle 'Answer confirmation required'
             Then Setting should be saved

        Scenario: user adds a new valid device
            Given I am on the end user device page
             When I add a new device with valid info
             Then a new device should be saved

        Scenario: user adds a new invalid device
            Given I am on the end user device page
             When I add a new device with invalid info
             Then a clear error message should appear
         
        Scenario: user edits a device and save
            Given I am on the end user device page
             When I edit and save device
             Then device should be edited and saved
      
        Scenario: user deletes a device
            Given I am on the end user device page
             When I delete a device
             Then a device should be deleted