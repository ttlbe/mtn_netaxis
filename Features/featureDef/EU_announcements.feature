@user_level
Feature: Manage announcements on end user level


        Scenario: Add invalid announcement
            Given I am on the end user announcements page
             When I add an invalid announcement
             Then I should see a clear error message

        Scenario: Add new announcement via recording
            Given I am on the end user announcements page
             When I add a new announcement
             Then I should see new annoucement

        Scenario: edit an announcement
            Given I am on the end user announcements page
             When I edit an announcement
             Then I should see the edited annoucement

        Scenario: Delete an announcement
            Given I am on the end user announcements page
             When I delete an announcement
             Then I should not see the annoucement