@group_level
Feature: manage group contacts

manage contacts on group level. 

        @addCC
        Scenario Outline: add new contacts to the company contact list
            Given I am on the contact list page
             When I click add new company contact
             When I provide the "<firstname>" "<lastname>" "<phonenr>" and click save
             Then "<firstname>" "<lastname>" should be added to the contact list

        Examples: contacts
                  | firstname   | lastname | phonenr        |
                  | Abdel       | Bannani  | 0472188837     |
                  | Gita        | Prajawati| 0032477748837  |
                  | Wouter      | Goris    | 0478666837     |
                  | Manu        | Eyckmans | 0472147437     |
                  | Peter       | Lissens  | 0472100037     |

        Scenario: add a new contact with illegal chars in the name fields
            Given I am on the contact list page
             When I click add new company contact
             When I provide illegal chars in the name fields and click 'save'
             Then a clear error message should be provided and the contact shouldn't have been created

        Scenario: delete a company contact
            Given I am on the contact list page
             When I click the actions icon of "Manu" "Eyckmans"
             When I click 'delete contact' and confirm the delete
             Then contact "Manu" "Eyckmans" should be deleted