@user_level
Feature: check phonebook feature on end user level

        Scenario Outline: add new contacts to the company contact list
            Given I am on the end user phonebook
             When I click add new company contact
             When I provide the "<firstname>" "<lastname>" "<phonenr>" and click save
             Then "<firstname>" "<lastname>" should be added to the contact list

        Examples: contacts
                  | firstname   | lastname | phonenr        |
                  | EndUser     | Test     | 0472166637     |

        Scenario: add a new contact with illegal chars in the name fields
            Given I am on the end user phonebook
             When I click add new company contact
             When I provide illegal chars in the name fields and click 'save'
             Then a clear error message should be provided and the contact shouldn't have been created

        Scenario: delete a company contact
            Given I am on the end user phonebook
             When I click the actions icon of "EndUser" "Test"
             When I click 'delete contact' and confirm the delete
             Then contact "EndUser" "Test" should be deleted