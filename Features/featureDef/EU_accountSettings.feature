@user_level
Feature: device manage on end user level


        Scenario: change language settings
            Given I am on the end user account settings page
             When I change language settings
             Then Language settings should be saved
     
        Scenario: change password
            Given I am on the end user account settings page
             When I change the password
             Then I should see a confirm popup