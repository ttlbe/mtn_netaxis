@group_level
Feature: manage the company announcements

        @createCA
        Scenario: Create a new company announcement
            Given I navigate to the company announcements page
             When I click the add new announcement button
             When I provide a name
             When I select 'choose message'
             When select the file I want to upload
             When I click the save button on the add new announcement form
             Then A new company announcement should be added
        @editCA
        Scenario: Edit an existing announcement
            Given I navigate to the company announcements page
             When I click the edit icon of the group announcement I want to edit
             When I change the settings and click the save button
             Then the announcement settings should be changed
        @deleteCA
        Scenario: Delete an announcement
            Given I navigate to the company announcements page
             When I click the delete icon of the group announcement I want to delete
             When I confirm the delete operation
             Then the respective group announcement should be deleted
