@tenant_level
Feature: Manage the licenses for groups

   As a group admin I should be able to assignt licenses to the end-users.
   The end-users should only be able to see the icons of certain services once those services are 
   actually assigned to the respective user.
        
        Scenario: create an end-user with no licenses and to continue to end-user portal
            Given I am on the home page on group level
             When I navigate to 'users'
             When I click 'add user'
             When I provide all the user details and click 'next'
             When I select a language from the list and click 'save user and skip next steps'
             When I click 'continue as end user..'
             Then I should be redirected to the correct end-users portal
             Then I should only see the 'call history' and 'phone book' tile
      
        Scenario: assign all the service packs to the end-user that was created in the previous scenario's
            Given I am on the 'users' page
             When I select the user I created without any licenses
             When I click 'edit' at the licenses section
             When I select all the licenses and save
             When I click 'continue as end user..'
             Then I should be redirected to the correct end-users portal
             Then I should see all the tiles that belong to the licenses