@tenant_level
Feature: Continue to group level and end user level as a tenant administrator

   As a tenant admin 
   I want to be able to Continue as a group administrator to the group level and from the group level to the end user level


        Scenario: from tenant level continue to the group level
            Given I am on tenant level
             When I open the details tab for one of the available groups
             When I click the continue as group link
             Then I should be on the group admins portal
             When I navigate to the users page
             When I select one of the available users
             When I click the continue as end user link
             Then I should be on the end users portal