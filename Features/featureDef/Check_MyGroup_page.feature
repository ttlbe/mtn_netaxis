@group_level
Feature: check 'my group' page

        Scenario: edit group admin details
            Given I navigate to 'my group'
             When I click 'edit' in the admin details section
             Then I should be able to edit the name and language settings for the group admin
        
        Scenario: change group admin password
            Given I navigate to 'my group'
             When I click 'change password' in the admin details section
             Then I should be able to edit the group admins password