@group_level
Feature: Manage call queues

        Scenario: add a new call queue
            Given I navigate to the call queue page
             When I click 'add a new call queue'
             When I provide all the required data and click 'save'
             Then a new call queue should be added

        Scenario: provide data which is not adhering to stipulations during queue creation process
            Given I navigate to the call queue page
             When I click 'add a new call queue'
             When I provide data which is not respecting stipulations and click 'save'
             Then a clear error message should be provided and the queue should not be added

        Scenario: edit an existing call queue
            Given I navigate to the call queue page
             When I open the details of an available call queue
             Then I should be able to edit the queue details and options

        Scenario: delete a call queue
            Given I navigate to the call queue page
             When I open the details of an available call queue
             When I click the delete icon and confirm the delete action
             Then the respective call queue should be deleted