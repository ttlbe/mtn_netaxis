
Feature: Test login feature

        @login
        Scenario: open the mtn selfcare portal and login as an enterprise admin
            Given I am on the mtn scp login page
             When I provide the correct credentials for tenant admin
             When I click the submit button
             Then I should be logged in


        Scenario: open the mtn selfcare portal and login as a group admin
            Given I am on the mtn scp login page
             When I provide valid group admin credentials
             When I click the submit button
             Then I should have access to the group admin portal
             

        Scenario: open the mtn selfcare portal and login as an end user
            Given I am on the mtn scp login page
             When I provide valid end user credentials
             When I click the submit button
             Then I should have access to the end users portal

