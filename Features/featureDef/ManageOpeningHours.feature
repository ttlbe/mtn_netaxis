@group_level
Feature: Managing opening days

        Scenario: Import the official bank holidays
            Given I am on the 'opening hours and closing days' page
             When I open the 'closing days' tab
             When click 'import official bank holidays'
             Then all the official bank holidays should be imported and auto set as closing days

        Scenario: manage opening hours
            Given I am on the 'opening hours and closing days' page
             When I open the 'opening hours' tab
             When click ' manage opening hours'
             Then I should be able to manage the opening hours

        Scenario: change opening hours
            Given I am on the 'opening hours and closing days' page
             When I open the 'opening hours' tab
             When click ' manage opening hours'
             When I change the opening hours
             Then I should be able to see changes
