@group_level
Feature: Managing the hunt groups
        
        Scenario: successfully add a new hunt group
            Given I navigate to the hunt groups page
             When I click 'add hunt group'
             When I provide all the required hunt data and click save
             Then a new hunt group should be added

        Scenario: add a hunt group with data which is not adhering to stipulation
            Given I navigate to the hunt groups page
             When I click 'add hunt group'
             When I provide data which is not adhering to stipulations and click save
             Then a clear error message should appear and the hunt group should not get created
        
        Scenario: edit an existing hunt group
            Given I navigate to the hunt groups page
             When I open the details of one of the available hunt groups
             Then possibilities should be provide to edit the hunt group name, manage the members and configuration settings

        Scenario: edit hunt group and save changes
            Given I navigate to the hunt groups page
            When  I open the details of one of the available hunt groups
            #When  I open the details of the edited hunt group
            When  I edit hunt group name, member and configuration settings and save
            Then  Hunt group should be updated accordingly

        Scenario:delete a hunt group
            Given I navigate to the hunt groups page
             When I open the details of the edited hunt group
             When I click 'delete hunt group'
             Then the respective hunt group should get deleted
