@group_level
Feature: Manage music on hold feature
             
        @uploadMoh
        Scenario: upload a file for music on hold
            Given I Navigate to the music on hold page
             When I click 'change'
             When I select upload a music file
             Then the file i uploaded should be set for music on hold

## 16/4/20 unavailable feature
        Scenario: select an audio file from free library for music on hold
            Given I Navigate to the music on hold page
             When I click 'change'
             When i select 'select from our library'
             Then the library file i selected should be set for music on hold

        Scenario: select an audio file was uploaded previously for music on hold
            Given I Navigate to the music on hold page
             When I click 'change'
             When i select 'select a file that was uploaded previously'
             Then the file i selected should be set for music on hold
