@tenant_level

Feature: check 'settings' page on tenant level
        
        Scenario: check if the admin details can be edited
            Given I am on the 'settings' page
             When  I click the 'edit' button in the administator details section
             When I change the admins details and click save
             Then the new details should be saved

        Scenario: check if password can be changed
            Given I am on the 'settings' page
             When I provide the old password and the new password and click save
             Then the new password should be saved
             Then I should be able to login with the new credentials

        Scenario: check if the 'phone numbers' tab contains the correct data
            Given I am on the 'settings' page
             When open the 'phone numbers' tab
             Then I should see an overview of all the available number ranges that were assigned to the respective tenant during provisioning