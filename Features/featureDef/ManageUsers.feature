@group_level
Feature: managing users on group level
#add, edit and delete users

        @addNewUser
        Scenario: Login as a group admin and successfully add a new user to the group
            Given I click the users link
             When I click the add user button
             When I provide all the required user data
             When I click next
             When I select a language
             When I skip the unnecessary steps and save the user
             Then A new user should be added to the group


        @justContinue
        Scenario: Try to continue without providing the required data
            Given I click the users link
             When I click the add user button
             When I try to continue without providing the required data
             Then a clear message should be provided under the required fields

        @editUser
        Scenario: Edit an existing user
            Given I click the users link
             When I open the details tab of the user I want to edit
             When I edit the users personal details
             When I edit the users number settings
             When I edit the users device  settings
             When I edit the user services
             Then the users details should be changed

        @deleteUser
        Scenario: Delete user(s)
            Given I click the users link
             When I select the user I want to delete
             When I click 'delete user' and confir the delete action
             Then The respective user should be deleted
