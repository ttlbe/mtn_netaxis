@group_level
Feature: Manage devices
adding, editing and deleting devices

        Scenario: add a device
            Given I navigate to 'my devices'
             When I click 'add a device'
             When I provide all the required data and click save
             Then a new device should be added
      
        Scenario: edit an existing device
            Given I navigate to 'my devices'
             When I click the edit icon of one of the available devices
             When I change the device settings and click 'save'
             Then the changes should get saved

        Scenario: delete a device
            Given I navigate to 'my devices'
             When I click the delete icon of one of the available devices and confirm the delete action
             Then the respective device should be deleted
    
        Scenario: delete a device which is assigned to a user
            Given I navigate to 'my devices'
             When I click the delete icon of one of the devices that are allready in use
             Then a clear error message should pop up and the device should not get deleted

