@group_level
Feature: Managing the pick up groups

        @addPG
        Scenario: add a new pickup group
            Given I am on the pickup groups page
             When I click the 'add pickup group' button, provide all the required data and click 'save'
             Then a new pickup group should be added

        Scenario: edit a pickup group
            Given I am on the pickup groups page
             When I open the details tab of one of the available pickup groups
             When edit the pickup group name, manage the members and change the configuration
             Then all the new settings should be set correctly

        Scenario: delete a pickup group
            Given I am on the pickup groups page
             When I open the details tab of one of the available pickup groups
             When click 'delete pickup group'
             Then the respective pickup group should be deleted
