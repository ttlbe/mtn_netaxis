@tenant_level
Feature: Manage the licenses for groups

   As a tenant admin I should be able to assignt licenses to my groups.
   The group admins shoulf only be able to see the icons of certain services once those services are 
   actually assigned to the respective group.

        
        Scenario: de-assign all licenses from a group
            Given I am on the home page on tenant level
             When I click one of the available groups to access the respective groups details
             When I click on the licenses tab and click 'edit'
             When I do not assign any of the available service packs to the respective group and click 'save'
             When I clikc the 'continue as group' link
             Then I should be redirected to the respective groups portal
             Then I should only see the following tile on home page .. # SKIP THIS STEP FOR NOW!