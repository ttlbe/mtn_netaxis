@tenant_level
Feature: check the advances search feature on tenant level
        
        as a tenant admin I should be able to search for users. 
        when a user is found it should be possible to click the user id and group id.
        if the user id is clicked the tenant admin should get redirected to the respective users portal
        and if the group id is clicked the tenant admin should get redirected to the group that contains the respective user

        
        Scenario: search for users by first name
            Given I am on the 'search' page
             When I provide the first name of one of the available end users
             Then an overview of all the user that have the respective first name should be shown

        
        Scenario: search for users by last name
            Given I am on the 'search' page
             When I provide the last name of one of the available end users
             Then an overview of all the user that have the respective last name should be shown

        Scenario: search for users by phone number
            Given I am on the 'search' page
             When I provide the phone number of one of the available end users
             Then an overview of all the user that have the respective phone number should be shown
        
        Scenario: search for users by email
            Given I am on the 'search' page
             When I provide the email of one of the available end users
             Then an overview of all the user that have the respective email should be shown

        Scenario: continue to the correct end user portal
            Given I am on the 'search' page
             When I have searched a user
             When I click the user ID
             Then I should get redirected to the portal of the respective user

        Scenario: continue to the portal of the correct group portal
            Given I am on the 'search' page
             When I have searched a user
             When I click the group ID
             Then I should get redirected to the portal of the group that contains the user I searched for