@tenant_level
Feature: Adding a new group to a tenant

        @addGrp
        Scenario: Login as a tenant admin and successfully add a new group to the tenant

            Given I am on the home page on tenant level
             When I click the add group button
             When I provide all the required data
             When I click the save button
             Then A new group should be added to the tenant

        @editGrp
        Scenario: Edit group details
            Given I am on the home page on tenant level
             When I navigate to the details overview of the group I want to edit and click 'edit'
             When I change the group details and click save
             # NOTE! GROUP ID SHOULD BE AUTO GENERATED AND NOT EDITABLE
             Then the new group details should saved


        @deleteGrp
        Scenario: delete a group
            Given I am on the home page on tenant level
             When I navigate to the details overview of the group I want to delete
             When I Click 'delete group' and confirm the delete action
             Then the respective group should be deleted


             